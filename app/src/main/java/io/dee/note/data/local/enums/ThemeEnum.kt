package io.dee.note.data.local.enums

enum class ThemeEnum(var theme: String) {
    Light("light"),
    Dark("Dark");
}
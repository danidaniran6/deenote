package io.dee.note.presentation.compose.archive_notes

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.dp
import io.dee.composenewsapp.ui.theme.DayNightSecondary
import io.dee.note.R
import io.dee.note.presentation.ui.theme.deeTypography

@Composable
fun ArchiveNoteTopBar(
    modifier: Modifier = Modifier,
    navigateUp: () -> Unit
) {
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically

    ) {
        Box(
            modifier = Modifier
                .background(
                    DayNightSecondary, shape = MaterialTheme.shapes.medium
                )
                .size(40.dp), contentAlignment = Alignment.Center
        ) {
            Icon(
                modifier = Modifier
                    .size(25.dp)
                    .clickable { navigateUp() },
                painter = painterResource(id = R.drawable.ic_arrow_left_linear),
                contentDescription = null
            )
        }
        Text(
            text = stringResource(id = R.string.archived_notes),
            modifier = Modifier.weight(1f),
            textAlign = TextAlign.Center,
            style = deeTypography.copy(
                fontWeight = FontWeight.Bold, fontSize = TextUnit(
                    16f,
                    TextUnitType.Sp
                )
            )
        )
        Spacer(modifier = Modifier.size(25.dp))
    }
}
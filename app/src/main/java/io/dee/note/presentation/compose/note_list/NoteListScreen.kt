package io.dee.note.presentation.compose.note_list

import androidx.annotation.DrawableRes
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import io.dee.composenewsapp.ui.theme.Black
import io.dee.composenewsapp.ui.theme.DayNightPrimary
import io.dee.composenewsapp.ui.theme.DayNightSecondary
import io.dee.note.R
import io.dee.note.presentation.compose.common.NoteList
import io.dee.note.presentation.compose.common.SearchBarSection
import io.dee.note.presentation.ui.theme.deeTypography

@Composable
fun NoteListScreen(
    modifier: Modifier = Modifier,
    state: NoteListState,
    onEvents: (NoteListEvents) -> Unit,
    emptyText: String = "No Note Available, ADD!"
) {


    Column(
        modifier = modifier
            .background(color = DayNightPrimary)
            .statusBarsPadding()
            .padding(top = 10.dp)
    ) {
        SearchBarSection(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 10.dp),
            text = state.searchQuery,
            onTextChanged = { onEvents(NoteListEvents.SearchNote(it)) },
            R.drawable.ic_search_linear
        )

        if (state.noteList.isNotEmpty())
            NoteList(modifier = Modifier
                .fillMaxSize()
                .padding(10.dp),
                list = state.noteList,
                onBookmarkClicked = { note ->
                    onEvents(NoteListEvents.Bookmark(note))
                },
                navigateToNoteDetails = { note ->
                    onEvents(NoteListEvents.EditNote(note))
                },
                showMoreOption = {
                    onEvents(it)
                })
        else EmptyScreen(
            modifier = Modifier.fillMaxSize(),
            emptyText = emptyText,
            R.drawable.ic_notes
        )
    }
}

@Composable
fun EmptyScreen(
    modifier: Modifier = Modifier,
    emptyText: String,
    @DrawableRes icon: Int
) {
    Box(
        modifier = modifier, contentAlignment = Alignment.Center
    ) {
        Box(
            modifier = Modifier
                .background(
                    color = DayNightSecondary, shape = MaterialTheme.shapes.medium
                )
                .fillMaxWidth(0.65f),
        ) {
            Row(
                modifier = Modifier.padding(20.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    modifier = Modifier.weight(1f),
                    text = emptyText,
                    color = Black,
                    style = deeTypography
                )
                Icon(
                    painter = painterResource(id = icon),
                    contentDescription = null,
                    tint = Black
                )
            }
        }
    }
}



package io.dee.note.presentation.compose.add_note.componenets

import androidx.compose.foundation.text.selection.TextSelectionColors
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import io.dee.composenewsapp.ui.theme.Black
import kotlinx.coroutines.delay

@Composable
fun NoteTextField(
    modifier: Modifier = Modifier,
    text: String,
    onTextChanged: (String) -> Unit,
    focusedByDefault: Boolean? = false,
    placeHolder: String
) {
    val focusRequester = remember { FocusRequester() }
    TextField(
        modifier = modifier.focusRequester(focusRequester),
        value = text,
        onValueChange = onTextChanged,
        placeholder = {
            Text(
                text = placeHolder,
                style = MaterialTheme.typography.titleMedium.copy(fontWeight = FontWeight.Normal)
            )
        },
        colors = TextFieldDefaults.colors(
            disabledContainerColor = Color.Transparent,
            errorContainerColor = Color.Transparent,
            focusedContainerColor = Color.Transparent,
            unfocusedContainerColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent,
            errorIndicatorColor = Color.Transparent,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            cursorColor = Black,
            selectionColors = TextSelectionColors(
                handleColor = Black,
                backgroundColor = Color.Blue
            ),
            focusedTextColor = Black,
            unfocusedTextColor = Black,
            focusedPlaceholderColor = Black.copy(alpha = 0.5f),
            unfocusedPlaceholderColor = Black.copy(alpha = 0.5f),
        ),
        textStyle = MaterialTheme.typography.titleMedium.copy(fontWeight = FontWeight.Normal),
    )

    if (focusedByDefault == true) {
        LaunchedEffect(Unit) {
            focusRequester.requestFocus()
        }
    }

}

@Composable
fun NoteTextField(
    modifier: Modifier = Modifier,
    text: TextFieldValue,
    onTextChanged: (TextFieldValue) -> Unit,
    focusedByDefault: Boolean? = false,
    placeHolder: String
) {
    val focusRequester = remember { FocusRequester() }
    TextField(
        modifier = modifier.focusRequester(focusRequester),
        value = text,
        onValueChange = onTextChanged,
        placeholder = {
            Text(
                text = placeHolder,
                style = MaterialTheme.typography.titleMedium.copy(fontWeight = FontWeight.Normal)
            )
        },
        colors = TextFieldDefaults.colors(
            disabledContainerColor = Color.Transparent,
            errorContainerColor = Color.Transparent,
            focusedContainerColor = Color.Transparent,
            unfocusedContainerColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent,
            errorIndicatorColor = Color.Transparent,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            cursorColor = Black,
            selectionColors = TextSelectionColors(
                handleColor = Black,
                backgroundColor = Color.Blue
            ),
            focusedTextColor = Black,
            unfocusedTextColor = Black,
            focusedPlaceholderColor = Black.copy(alpha = 0.5f),
            unfocusedPlaceholderColor = Black.copy(alpha = 0.5f),
        ),
        textStyle = MaterialTheme.typography.titleMedium.copy(fontWeight = FontWeight.Normal),
    )

    if (focusedByDefault == true) {
        LaunchedEffect(Unit) {
            focusRequester.requestFocus()
        }
    }
}
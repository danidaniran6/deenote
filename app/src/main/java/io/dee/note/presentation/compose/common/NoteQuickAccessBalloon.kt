package io.dee.note.presentation.compose.common


import android.content.res.Configuration
import androidx.annotation.DrawableRes
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import io.dee.composenewsapp.ui.theme.Black
import io.dee.composenewsapp.ui.theme.DayNightPrimary
import io.dee.composenewsapp.ui.theme.Red
import io.dee.composenewsapp.ui.theme.DayNightSecondary
import io.dee.note.R
import io.dee.note.data.local.database.NoteData
import io.dee.note.presentation.compose.note_list.NoteListEvents
import io.dee.note.presentation.ui.theme.deeTypography

@Composable
fun ArchivedNoteQuickAccessBalloon(
    note: NoteData, onEvent: (NoteListEvents) -> Unit
) {
    Column(
        verticalArrangement = Arrangement.spacedBy(5.dp)
    ) {
        NoteQuickAccessBalloonItem(
            modifier = Modifier.clickable { onEvent(NoteListEvents.EditNote(note.copy())) },
            stringResource(id = R.string.edit_note),
            R.drawable.ic_edit_linear,
            iconTint = Black
        )
        NoteQuickAccessBalloonItem(
            modifier = Modifier.clickable { onEvent(NoteListEvents.ArchiveNote(note.copy())) },
            "Remove From Archive",
            R.drawable.ic_trash_linear,
            iconTint = Red
        )
    }

}

@Composable
fun NoteQuickAccessBalloon(
    note: NoteData, onEvent: (NoteListEvents) -> Unit
) {
    Column(

        verticalArrangement = Arrangement.spacedBy(5.dp)
    ) {
        NoteQuickAccessBalloonItem(
            modifier = Modifier.clickable { onEvent(NoteListEvents.EditNote(note.copy())) },
            stringResource(id = R.string.edit_note),
            R.drawable.ic_edit_linear,
            iconTint = Black
        )
        NoteQuickAccessBalloonItem(
            modifier = Modifier.clickable { onEvent(NoteListEvents.Bookmark(note.copy())) },
            "Bookmark",
            if (note.isBookmarked) R.drawable.ic_bookmark_bold else R.drawable.ic_bookmark_linear,
            iconTint = Black
        )
        NoteQuickAccessBalloonItem(
            modifier = Modifier.clickable { onEvent(NoteListEvents.ArchiveNote(note.copy())) },
            stringResource(id = R.string.archive),
            R.drawable.ic_archive_linear,
            iconTint = Black
        )
        NoteQuickAccessBalloonItem(
            modifier = Modifier.clickable { onEvent(NoteListEvents.ShowDeleteDialog(note.copy())) },
            stringResource(id = R.string.delete),
            R.drawable.ic_trash_linear,
            iconTint = Red
        )

    }

}

@Composable
fun NoteBalloon(
    note: NoteData, onEvent: (NoteListEvents) -> Unit
) {
    Column(
        modifier = Modifier
            .width(180.dp)
            .background(DayNightPrimary, shape = MaterialTheme.shapes.medium)
            .padding(10.dp)
    ) {

        if (!note.isArchived) {
            NoteQuickAccessBalloon(note = note, onEvent = onEvent)
        } else {
            ArchivedNoteQuickAccessBalloon(note = note, onEvent = onEvent)
        }

    }

}

@Composable
fun NoteQuickAccessBalloonItem(
    modifier: Modifier, text: String, @DrawableRes icon: Int, iconTint: Color
) {
    Card(
        modifier = Modifier.then(modifier), colors = CardDefaults.cardColors(
            containerColor = DayNightSecondary
        ), shape = MaterialTheme.shapes.medium
    ) {
        Row(
            modifier = Modifier.padding(10.dp), verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = text,
                modifier = Modifier.weight(1f),
                color = iconTint,
                style = deeTypography.copy(fontWeight = FontWeight.Bold)
            )
            Icon(
                modifier = Modifier.size(20.dp),
                painter = painterResource(id = icon),
                contentDescription = null,
                tint = iconTint
            )
        }

    }
}

@Preview()
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewBalloon() {
    NoteBalloon(note = NoteData(isArchived = false), {})
}

@Preview()
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewBalloonForArchived() {
    NoteBalloon(note = NoteData(isArchived = true), {})
}
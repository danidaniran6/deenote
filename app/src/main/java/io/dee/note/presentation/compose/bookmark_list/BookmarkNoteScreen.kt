package io.dee.note.presentation.compose.bookmark_list

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import io.dee.note.presentation.compose.note_list.NoteListEvents
import io.dee.note.presentation.compose.note_list.NoteListScreen
import io.dee.note.presentation.compose.note_list.NoteListState

@Composable
fun BookmarkedNoteList(
    state: NoteListState,
    onEvents: (NoteListEvents) -> Unit
) {
    NoteListScreen(
        modifier = Modifier
            .fillMaxSize()
            .padding(top = 10.dp)
            .statusBarsPadding(),
        state = state,
        onEvents = onEvents
    )
}
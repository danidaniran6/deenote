package io.dee.note.presentation.compose.bookmark_list

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import dagger.hilt.android.lifecycle.HiltViewModel
import io.dee.note.data.local.database.NoteData
import io.dee.note.data.repository.note.BookmarkedNotesRepository
import io.dee.note.presentation.compose.home_screen.EditNoteScreen
import io.dee.note.presentation.compose.note_list.NoteListEvents
import io.dee.note.presentation.compose.note_list.NoteListState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BookmarkedNoteListViewModel
@Inject constructor(
    private val repository: BookmarkedNotesRepository
) : ViewModel() {


    var navController: NavController? = null

    var state by mutableStateOf(NoteListState())
        private set

    init {
        getAllFavouritesNotes()
    }

    private fun getAllFavouritesNotes(searchQuery: String = "") = viewModelScope.launch {
        repository.getAllBookmarkedNotesAsAFlow().onEach {
            val list = if (searchQuery.isNotEmpty()) it.filter {
                it.title.contains(searchQuery, true) || it.description.contains(searchQuery, true)
            } else it
            state = state.copy(noteList = list, searchQuery = searchQuery)
        }.launchIn(viewModelScope)
    }


    fun onEvent(events: NoteListEvents) {
        when (events) {

            is NoteListEvents.SearchNote -> {
                getAllFavouritesNotes(searchQuery = events.searchQuery)
            }

            is NoteListEvents.EditNote -> {
                navController?.navigate(
                    EditNoteScreen(
                        events.note.dId
                    )
                )
            }

            is NoteListEvents.Bookmark -> {
                removeNoteFavourite(events.note)
            }

            is NoteListEvents.DeleteNote -> {
                deleteANote(events.note)
            }

            is NoteListEvents.ArchiveNote -> {
                archiveNote(note = events.note)
            }

            else -> {}
        }


    }

    fun removeNoteFavourite(note: NoteData) = viewModelScope.launch {
        repository.upsertNote(note.apply {
            this.isBookmarked = false
        })
    }

    fun archiveNote(note: NoteData) = viewModelScope.launch {
        repository.upsertNote(note.apply {
            this.isArchived = true
        })
    }


    private var _noteList: MutableStateFlow<List<NoteData>> = MutableStateFlow(mutableListOf())
    val noteList = _noteList.asStateFlow()


    fun deleteANote(note: NoteData) = viewModelScope.launch {
        repository.deleteANote(note)
    }
}
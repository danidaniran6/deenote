package io.dee.note.data.local.database.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import io.dee.note.data.local.database.NoteData
import kotlinx.coroutines.flow.Flow

@Dao
interface NoteDataDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertANewNote(noteData: NoteData)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsertNote(noteData: NoteData)

    @Delete
    suspend fun deleteANote(noteData: NoteData)

    @Transaction
    suspend fun deleteTheInvalidNote() {
        val invalidNote = getLastInsertedNote()
        invalidNote?.let {
            if (invalidNote.title.isEmpty() && invalidNote.description.isEmpty()) {
                deleteANote(invalidNote)
            }
        }
    }

    @Query("select * from notedata where isArchived is 0  order by createdAt Desc limit 1")
    suspend fun getLastInsertedNote(): NoteData?

    @Query("select * from notedata where dId=:id and isArchived is 0 limit 1")
    suspend fun getNoteById(id:Int): NoteData?


    @Query("select * from notedata where isArchived is 0  order by createdAt Desc")
    suspend fun getAllNotesAsAList(): List<NoteData>

    @Query("select * from notedata where isArchived is 0  order by createdAt Desc")
     fun getAllNotesAsALiveData(): LiveData<List<NoteData>>


    @Query("select * from notedata where isArchived is 0  order by createdAt Desc")
    fun getAllNotesAsAFlow(): Flow<List<NoteData>>


    @Query("select * from notedata where isBookmarked is 1 and isArchived is 0 order by createdAt Desc")
    suspend fun getAllBookmarkedNotesAsAList(): List<NoteData>


    @Query("select * from notedata where isBookmarked is 1 and isArchived is 0  order by createdAt Desc")
    fun getAllBookmarkedNotesAsAFlow(): Flow<List<NoteData>>


    @Query("select * from notedata where isArchived is 1 order by createdAt Desc")
    suspend fun getAllArchivedNotesAsAList(): List<NoteData>


    @Query("select * from notedata where isArchived is 1 order by createdAt Desc")
    fun getAllArchivedNotesAsAFlow(): Flow<List<NoteData>>
}
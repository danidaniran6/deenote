package io.dee.note.presentation.compose.add_note

data class AddNoteState(
    val titleText: String = "",
    val bodyText: String = "",
    val isBookmarked: Boolean = false
)

package io.dee.note.app

import android.app.Application
import com.google.android.material.color.DynamicColors
import com.google.firebase.FirebaseApp
import dagger.hilt.android.HiltAndroidApp
import io.dee.note.utils.UiModeHelper

@HiltAndroidApp
class DeeNote : Application() {
    override fun onCreate() {
        super.onCreate()
        DynamicColors.applyToActivitiesIfAvailable(this)
        UiModeHelper.initUiMode(this)
        FirebaseApp.initializeApp(this)
    }
}
package io.dee.note.data.repository.note

import io.dee.note.data.local.database.NoteData

interface AddNoteRepository {
    suspend fun insertANewNote(note: NoteData)
}
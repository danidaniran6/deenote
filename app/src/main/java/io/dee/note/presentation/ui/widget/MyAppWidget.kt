package io.dee.note.presentation.ui.widget;


import android.appwidget.AppWidgetManager
import android.content.Context
import androidx.compose.runtime.Composable
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.dp
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.glance.ColorFilter
import androidx.glance.GlanceId
import androidx.glance.GlanceModifier
import androidx.glance.GlanceTheme
import androidx.glance.Image
import androidx.glance.ImageProvider
import androidx.glance.LocalContext
import androidx.glance.action.ActionParameters
import androidx.glance.action.actionParametersOf
import androidx.glance.action.actionStartActivity
import androidx.glance.action.clickable
import androidx.glance.appwidget.GlanceAppWidget
import androidx.glance.appwidget.GlanceAppWidgetManager
import androidx.glance.appwidget.GlanceAppWidgetReceiver
import androidx.glance.appwidget.lazy.LazyColumn
import androidx.glance.appwidget.provideContent
import androidx.glance.appwidget.state.updateAppWidgetState
import androidx.glance.background
import androidx.glance.currentState
import androidx.glance.layout.Alignment
import androidx.glance.layout.Column
import androidx.glance.layout.Row
import androidx.glance.layout.fillMaxSize
import androidx.glance.layout.fillMaxWidth
import androidx.glance.layout.padding
import androidx.glance.layout.size
import androidx.glance.layout.wrapContentWidth
import androidx.glance.state.PreferencesGlanceStateDefinition
import androidx.glance.text.FontWeight
import androidx.glance.text.Text
import androidx.glance.text.TextStyle
import co.okex.app.utils.formatDisplayNameDate
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import io.dee.note.R
import io.dee.note.data.local.database.AppDb
import io.dee.note.data.local.database.NoteData
import io.dee.note.presentation.ui.activity.main.MainActivity
import io.dee.note.presentation.ui.theme.MyAppWidgetGlanceColorScheme
import io.dee.note.utils.IntentActionUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.Date
import javax.inject.Inject

private val destinationKey = ActionParameters.Key<Int>(
    MainActivity.KEY_DESTINATION
)
private val widgetAction = ActionParameters.Key<String>(
    MainActivity.WidgetAction
)


class MyAppWidget() : GlanceAppWidget() {


    override suspend fun provideGlance(context: Context, id: GlanceId) {
        // Load data needed to render the AppWidget.
        // Use `withContext` to switch to another thread for long running
        // operations.
        provideContent {
            val prefs = currentState<Preferences>()
            val deserializedList = prefs[stringPreferencesKey("notes")] ?: ""
            val list = try {
                Gson().fromJson(
                    deserializedList, Array<NoteData>::class.java
                ).toList()
            } catch (e: Exception) {
                mutableListOf()
            }
            MyContent(list)
        }


    }

}

@Composable
private fun MyContent(noteList: List<NoteData>) {
    val context = LocalContext.current
    GlanceTheme(colors = MyAppWidgetGlanceColorScheme.colors) {

        Column(
            modifier = GlanceModifier.fillMaxSize().background(GlanceTheme.colors.primary)
                .padding(15.dp)
        ) {
            Row(
                modifier = GlanceModifier.fillMaxWidth().padding(10.dp),
                verticalAlignment = Alignment.Vertical.CenterVertically
            ) {
                Text(
                    modifier = GlanceModifier.defaultWeight(),
                    text = "N o t e s", style =
                    TextStyle(
                        color = GlanceTheme.colors.background,
                        fontWeight = FontWeight.Bold,
                        fontSize = TextUnit(
                            18f,
                            TextUnitType.Sp
                        ),

                        )
                )
                Image(
                    modifier = GlanceModifier.size(20.dp)
                        .clickable(
                            onClick = actionStartActivity<MainActivity>(
                                parameters = actionParametersOf(widgetAction to context.packageName + IntentActionUtil.ADD_NOTE)
                            )
                        ),
                    provider = ImageProvider(R.drawable.ic_add_note_linear),
                    contentDescription = null,
                    colorFilter = ColorFilter.tint(GlanceTheme.colors.background)

                )
            }
            LazyColumn() {
                items(noteList.size) { index ->
                    Column(
                        modifier = GlanceModifier.fillMaxWidth().padding(5.dp),
                    ) {
                        WidgetNoteItem(note = noteList[index])
                    }

                }

            }
        }


    }


}

@Composable
fun WidgetNoteItem(
    note: NoteData
) {
    val context = LocalContext.current

    Column(
        modifier = GlanceModifier.fillMaxWidth()
            .background(imageProvider = ImageProvider(R.drawable.round_4_secondary))
            .padding(10.dp).clickable(
                onClick = actionStartActivity<MainActivity>(
                    actionParametersOf(
                        destinationKey to note.dId,
                        widgetAction to context.packageName + IntentActionUtil.OPEN_NOTE
                    )
                )
            )
    ) {
        if (note.title.isNotEmpty()) Text(
            text = note.title,
            modifier = GlanceModifier.wrapContentWidth().padding(bottom = 5.dp),
            style = TextStyle(
                color = GlanceTheme.colors.background,
                fontWeight = FontWeight.Bold,
                fontSize = TextUnit(
                    16f,
                    TextUnitType.Sp
                )
            )
        )
        if (note.description.isNotEmpty()) Text(
            text = note.description,
            modifier = GlanceModifier.fillMaxWidth().padding(bottom = 5.dp),
            maxLines = 4,
            style = TextStyle(
                color = GlanceTheme.colors.background, fontSize = TextUnit(
                    14f,
                    TextUnitType.Sp
                )
            )
        )
        Text(
            text = Date(note.createdAt).formatDisplayNameDate(),
            modifier = GlanceModifier.fillMaxWidth(),
            style = TextStyle(
                color = GlanceTheme.colors.background, TextUnit(
                    10f,
                    TextUnitType.Sp
                )
            )
        )


    }

}


@AndroidEntryPoint
class MyAppWidgetReceiver : GlanceAppWidgetReceiver() {
    @Inject
    lateinit var db: AppDb

    // Let MyAppWidgetReceiver know which GlanceAppWidget to use
    override var glanceAppWidget: GlanceAppWidget = MyAppWidget()


    private fun observeData(context: Context) {

        CoroutineScope(Dispatchers.IO).launch {

            val allNotes = db.noteDataDao.getAllNotesAsAList()

            val serializedList = Gson().toJson(allNotes)

            val glanceId =
                GlanceAppWidgetManager(context).getGlanceIds(MyAppWidget::class.java).firstOrNull()

            if (glanceId != null) {
                updateAppWidgetState(context, PreferencesGlanceStateDefinition, glanceId) { prefs ->
                    prefs.toMutablePreferences()
                    prefs.toMutablePreferences().apply {
                        set(stringPreferencesKey("notes"), serializedList)
                    }
                }
                glanceAppWidget.update(context, glanceId)
            }
        }
    }

    override fun onUpdate(
        context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray
    ) {
        observeData(context = context)
        super.onUpdate(context, appWidgetManager, appWidgetIds)
    }

}
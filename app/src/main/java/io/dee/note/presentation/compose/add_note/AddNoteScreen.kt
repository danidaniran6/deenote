package io.dee.note.presentation.compose.add_note

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import io.dee.note.presentation.compose.common.SetTimerAlertDialog

@Composable
fun AddNoteScreen(
    viewModel: AddNoteViewModel,
    navigateUp: () -> Unit
) {
    val context = LocalContext.current
    BackHandler(true) {
        viewModel.onEvent(AddNoteEvents.AddNoteToDb(context))
        navigateUp()
    }
    viewModel.showSetTimerDialog?.let {
        SetTimerAlertDialog(
            reminderTime = viewModel.state.reminder?.dateToRemind,
            onDateSelected = { calendar ->
                viewModel.onEvent(AddNoteEvents.UpdateNoteTimer(calendar))
            })
    }
    AddNoteScreenBody(
        state = viewModel.state,
        onEvent = viewModel::onEvent,
        navigateUp = {
            viewModel.onEvent(AddNoteEvents.AddNoteToDb(context))
            navigateUp()
        }
    )
}
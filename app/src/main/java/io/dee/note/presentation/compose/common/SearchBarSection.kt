package io.dee.note.presentation.compose.common

import android.content.res.Configuration
import androidx.annotation.DrawableRes
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import io.dee.composenewsapp.ui.theme.Black
import io.dee.composenewsapp.ui.theme.DayNightSecondary
import io.dee.note.R

@Composable
fun SearchBarSection(
    modifier: Modifier = Modifier,
    text: String,
    onTextChanged: (String) -> Unit,
    @DrawableRes trailingIcon: Int
) {

    TextField(
        modifier = modifier,
        value = text,
        onValueChange = { onTextChanged(it) },
        trailingIcon = {
            Icon(
                modifier = Modifier.size(20.dp),
                painter = painterResource(id = trailingIcon), contentDescription = null
            )
        },
        placeholder = {
            Text(
                text = "search",
                color = Black.copy(alpha = 0.5f),
                style = MaterialTheme.typography.bodyMedium
            )
        },
        colors = TextFieldDefaults.colors(
            focusedTextColor = Black,
            focusedContainerColor = DayNightSecondary,
            unfocusedContainerColor = DayNightSecondary,
            unfocusedIndicatorColor = Color.Transparent,
            focusedIndicatorColor = Color.Transparent,
            errorIndicatorColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent,
        ),
        textStyle = MaterialTheme.typography.bodyMedium,
        shape = MaterialTheme.shapes.medium
    )

}

@Preview(showBackground = true)
@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewSearchBarSection() {
    Surface {

        SearchBarSection(
            modifier = Modifier.fillMaxWidth(),
            text = "",
            onTextChanged = {},
            trailingIcon = R.drawable.ic_search_linear
        )
    }
}
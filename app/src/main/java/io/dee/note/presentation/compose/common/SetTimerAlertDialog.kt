package io.dee.note.presentation.compose.common

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.dp
import co.okex.app.utils.formatDisplayDate
import io.dee.composenewsapp.ui.theme.Black
import io.dee.composenewsapp.ui.theme.DayNightPrimary
import io.dee.composenewsapp.ui.theme.Red
import io.dee.composenewsapp.ui.theme.DayNightSecondary
import io.dee.note.presentation.ui.theme.deeTypography
import io.dee.note.utils.make2Digits
import java.util.Calendar

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SetTimerAlertDialog(
    reminderTime: Calendar? = null,
    onDateSelected: (Calendar?) -> Unit,
) {
    var showTimerPicker by remember {
        mutableStateOf<Boolean?>(null)
    }
    var showDatePicker by remember {
        mutableStateOf<Boolean?>(null)
    }
    var calendar: Calendar? by remember {
        mutableStateOf(reminderTime)
    }

    val selectedTime by remember {
        derivedStateOf {
            calendar?.let { displayTime(it) }
        }
    }

    val selectedDate by remember {
        derivedStateOf {
            calendar?.let { it.time.formatDisplayDate() }
        }
    }

    AlertDialog(onDismissRequest = { }, confirmButton = {
        TextButton(
            onClick = { onDateSelected(calendar) }, colors = ButtonDefaults.buttonColors(
                containerColor = DayNightSecondary, contentColor = Black
            ), contentPadding = PaddingValues(horizontal = 20.dp)
        ) {
            Text(text = "Save")
        }
    }, dismissButton = {
        if (calendar != null)
            TextButton(
                onClick = { calendar = null }, colors = ButtonDefaults.buttonColors(
                    containerColor = Color.Transparent, contentColor = Black.copy(0.5f)
                )
            ) {
                Text(text = "Delete", color = Red)
            }
        TextButton(
            onClick = { onDateSelected(reminderTime) }, colors = ButtonDefaults.buttonColors(
                containerColor = Color.Transparent, contentColor = Black.copy(0.5f)
            )
        ) {
            Text(text = "Dismiss", color = Black.copy(0.8f))
        }
    }, text = {

        if (showDatePicker == true)
            DatePickerDialog(
                initialTime = calendar ?: Calendar.getInstance(),
                onDateSelected = { year, month, day ->
                    if (year != null && month != null && day != null) {
                        if (calendar == null) calendar =
                            Calendar.getInstance().also { it.timeInMillis += 5 * 60 * 1000 }
                        calendar?.set(Calendar.YEAR, year)
                        calendar?.set(Calendar.MONTH, month)
                        calendar?.set(Calendar.DAY_OF_MONTH, day)
                    }
                    showDatePicker = null
                })

        if (showTimerPicker == true) {
            TimePickerDialog(
                initialTime = calendar ?: Calendar.getInstance(),
                onTimeSelected = { hour, minute ->
                    if (hour != null && minute != null) {
                        val cal = Calendar.getInstance().apply {
                            set(Calendar.HOUR, hour)
                            set(Calendar.MINUTE, minute)
                        }
                        calendar = cal
                    }

                    showTimerPicker = null

                })
        }
        Column(
            modifier = Modifier.fillMaxWidth()

        ) {

            Text(
                modifier = Modifier.padding(horizontal = 5.dp),
                text = "Set Date",
                style = deeTypography.copy(fontSize = TextUnit(16f, TextUnitType.Sp)),
                color = Black
            )
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 10.dp)
                    .background(
                        color = DayNightSecondary, shape = MaterialTheme.shapes.medium
                    ), contentAlignment = Alignment.Center
            ) {

                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(15.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        modifier = Modifier
                            .weight(1f)
                            .clickable {
                                showDatePicker = true
                            },
                        text = selectedDate ?: "Set Date",
                        style = MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Normal),
                        color = Black.copy(alpha = 0.5f)
                    )


                }
            }

            Text(
                modifier = Modifier
                    .padding(horizontal = 5.dp)
                    .padding(top = 20.dp),
                text = "Set Time",
                style = deeTypography.copy(
                    fontSize = TextUnit(
                        16f, TextUnitType.Sp
                    ), color = if (selectedDate != null) Black else Black.copy(0.3f)
                )
            )
            Box(
                modifier = Modifier
                    .fillMaxWidth()

                    .padding(top = 10.dp)
                    .background(
                        color = if (selectedDate != null) DayNightSecondary else DayNightSecondary.copy(0.7f),
                        shape = MaterialTheme.shapes.medium
                    ), contentAlignment = Alignment.Center
            ) {

                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(15.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        modifier = Modifier
                            .weight(1f)
                            .clickable(enabled = selectedDate != null) {
                                showTimerPicker = true
                            },
                        text = selectedTime ?: "Set Time",
                        style = MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Normal),
                        color = Black.copy(alpha = if (selectedDate != null) 0.5f else 0.1f)
                    )


                }
            }

        }
    }, containerColor = DayNightPrimary)
}

@OptIn(ExperimentalMaterial3Api::class)
fun displayTime(calendar: Calendar): String {
    val hour = calendar.get(Calendar.HOUR)
    val minute = calendar.get(Calendar.MINUTE)
    val hourString = hour.toString().make2Digits()
    val minuteString = minute.toString().make2Digits()
    return "$hourString:$minuteString"

}

@Preview(showBackground = true)
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun Preivew() {
    SetTimerAlertDialog(
        onDateSelected = {

        }
    )
}
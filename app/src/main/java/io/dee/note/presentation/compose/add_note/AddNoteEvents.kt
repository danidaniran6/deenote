package io.dee.note.presentation.compose.add_note

import android.content.Context
import java.util.Calendar

sealed class AddNoteEvents {
    data class UpdateTitleText(val title: String) : AddNoteEvents()
    data class UpdateBodyText(val body: String) : AddNoteEvents()
    data class UpdateNoteTimer(val calendar: Calendar?) : AddNoteEvents()
    data class AddNoteToDb(val context: Context) : AddNoteEvents()
    data object UpdateBookmarkState : AddNoteEvents()


    data object ShowTimerDialog : AddNoteEvents()
    data object RemoveTimerDialog : AddNoteEvents()
}


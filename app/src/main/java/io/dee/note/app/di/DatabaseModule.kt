package io.dee.note.app.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import io.dee.note.data.local.database.AppDb
import io.dee.note.data.local.database.MIGRATION_2_3
import io.dee.note.data.local.database.Migration_3_4

import io.dee.note.data.local.database.MyDatabaseCallback
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    @Singleton
    fun provideDataBase(@ApplicationContext context: Context): AppDb {
        return Room.databaseBuilder(
            context,
            AppDb::class.java,
            "DeeNoteDb"
        )
            .addCallback(
                MyDatabaseCallback(
                    context
                )
            )
            .addMigrations(MIGRATION_2_3, Migration_3_4)
            .fallbackToDestructiveMigration()
            .build()
    }
}
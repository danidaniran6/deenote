package io.dee.note.presentation.compose.archive_notes.components

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import io.dee.note.R
import io.dee.note.presentation.compose.note_list.NoteListEvents
import io.dee.note.presentation.compose.note_list.NoteListScreen
import io.dee.note.presentation.compose.note_list.NoteListState

@Composable
fun ArchiveNoteList(
    modifier: Modifier = Modifier,
    state: NoteListState,
    onEvents: (NoteListEvents) -> Unit
) {
    NoteListScreen(
        modifier = modifier, state = state, onEvents = onEvents,
        emptyText = stringResource(id = R.string.no_archived_note_available)
    )
}

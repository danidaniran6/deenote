package io.dee.note.presentation.compose.common

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.BasicAlertDialog
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.DatePicker
import androidx.compose.material3.DatePickerDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.SelectableDates
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.rememberDatePickerState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import io.dee.composenewsapp.ui.theme.Black
import io.dee.composenewsapp.ui.theme.DayNightPrimary
import io.dee.composenewsapp.ui.theme.DayNightSecondary
import io.dee.composenewsapp.ui.theme.White
import java.util.Calendar

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DatePickerDialog(
    initialTime: Calendar, onDateSelected: (year: Int?, month: Int?, day: Int?) -> Unit
) {
    val datePickerState =
        rememberDatePickerState(initialSelectedDateMillis = initialTime.timeInMillis,
            selectableDates =
            object : SelectableDates {
                override fun isSelectableDate(utcTimeMillis: Long): Boolean {
                    val today = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
                    val utcCalendar =
                        Calendar.getInstance().also { it.timeInMillis = utcTimeMillis }
                    val day = utcCalendar.get(Calendar.DAY_OF_MONTH)
                    return today <= day
                }

                override fun isSelectableYear(year: Int): Boolean {
                    return year == Calendar.getInstance().get(Calendar.YEAR)
                }
            })
    BasicAlertDialog(modifier = Modifier.background(
        DayNightPrimary,
        shape = MaterialTheme.shapes.medium
    ),
        onDismissRequest = {

        },
        content = {
            Column {
                DatePicker(
                    modifier = Modifier.padding(10.dp),
                    state = datePickerState,
                    colors = DatePickerDefaults.colors(
                        containerColor = DayNightPrimary,
                        titleContentColor = Black,
                        dayContentColor = Black,
                        yearContentColor = Black,
                        disabledDayContentColor = Black.copy(alpha = 0.3f),
                        selectedDayContainerColor = Black,
                        selectedDayContentColor = White
                    )
                )
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(15.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.spacedBy(5.dp, Alignment.End),
                ) {
                    TextButton(
                        onClick = {
                            onDateSelected(null, null, null)
                        }, colors = ButtonDefaults.buttonColors(
                            contentColor = Black, containerColor = Color.Transparent
                        )
                    ) { Text(text = "Dismiss") }

                    TextButton(
                        onClick = {
                            datePickerState.selectedDateMillis?.let { selectedTime ->
                                val c =
                                    Calendar.getInstance().also { it.timeInMillis = selectedTime }
                                onDateSelected(
                                    c.get(Calendar.YEAR),
                                    c.get(Calendar.MONTH),
                                    c.get(Calendar.DAY_OF_MONTH)
                                )
                            }

                        }, colors = ButtonDefaults.buttonColors(
                            contentColor = Black, containerColor = DayNightSecondary
                        )
                    ) { Text(text = "Apply", modifier = Modifier.padding(horizontal = 15.dp)) }
                }
            }
        })
}


@Preview(showBackground = true)
@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewDatePickerDialog() {
    DatePickerDialog(initialTime = Calendar.getInstance(), { _, _, _ -> })
}
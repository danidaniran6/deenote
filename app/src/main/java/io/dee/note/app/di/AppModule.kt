package io.dee.note.app.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.dee.note.app.DeeNote
import io.dee.note.data.local.database.AppDb
import io.dee.note.data.repository.note.AddNoteRepository
import io.dee.note.data.repository.note.AddNoteRepositoryImpl
import io.dee.note.data.repository.note.ArchivedNoteListRepository
import io.dee.note.data.repository.note.ArchivedNoteListRepositoryImpl
import io.dee.note.data.repository.note.BookmarkedNotesRepository
import io.dee.note.data.repository.note.BookmarkedNotesRepositoryImpl
import io.dee.note.data.repository.note.EditNoteRepository
import io.dee.note.data.repository.note.EditNoteRepositoryImpl
import io.dee.note.data.repository.note.NoteListRepository
import io.dee.note.data.repository.note.NoteListRepositoryImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {


    @Provides
    @Singleton
    fun provideNoteListRepository(appDb: AppDb): NoteListRepository {
        return NoteListRepositoryImpl(appDb)
    }

    @Provides
    @Singleton
    fun provideAddNoteRepository(appDb: AppDb): AddNoteRepository {
        return AddNoteRepositoryImpl(appDb)
    }

    @Provides
    @Singleton
    fun provideEditNoteRepository(appDb: AppDb): EditNoteRepository {
        return EditNoteRepositoryImpl(appDb)
    }

    @Provides
    @Singleton
    fun provideBookmarkedNotesRepository(appDb: AppDb): BookmarkedNotesRepository {
        return BookmarkedNotesRepositoryImpl(appDb)
    }

    @Provides
    @Singleton
    fun provideArchivedNotesRepository(appDb: AppDb): ArchivedNoteListRepository {
        return ArchivedNoteListRepositoryImpl(appDb)
    }
}
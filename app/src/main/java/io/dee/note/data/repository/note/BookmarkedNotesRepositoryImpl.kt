package io.dee.note.data.repository.note

import io.dee.note.data.local.database.AppDb
import io.dee.note.data.local.database.NoteData
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class BookmarkedNotesRepositoryImpl
@Inject constructor(
    private val appDb: AppDb
) : BookmarkedNotesRepository {
    override suspend fun upsertNote(note: NoteData) {
        appDb.noteDataDao.upsertNote(note)
    }

    override suspend fun getAllBookmarkedNotesAsAList(): List<NoteData> {
        return appDb.noteDataDao.getAllBookmarkedNotesAsAList()
    }

    override fun getAllBookmarkedNotesAsAFlow(): Flow<List<NoteData>> {
        return appDb.noteDataDao.getAllBookmarkedNotesAsAFlow()
    }

    override suspend fun deleteANote(note: NoteData) {
        appDb.noteDataDao.deleteANote(note)
    }
}
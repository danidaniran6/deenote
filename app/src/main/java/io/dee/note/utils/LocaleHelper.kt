//package io.dee.note.utils
//
//import android.annotation.TargetApi
//import android.app.Activity
//import android.content.Context
//import android.content.SharedPreferences
//import android.content.res.Configuration
//import android.content.res.Resources
//import android.os.Build
//import android.view.View
//import androidx.core.text.layoutDirection
//import io.dee.note.app.IS_CONFIGURATION_CHANGED
//import java.util.Locale
//
///** this  object class is used to handle locale changes
// * stores the last selected locale*/
//class LocaleHelper {
//    private lateinit var baseContext: Context
//
//    fun init(context: Context) {
//        baseContext = context
//    }
//
//    fun setLanguage(activity: Activity, language: String) {
//        persist(language)
//        baseContext = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            updateResources(language)
//        } else updateResourcesLegacy(language)
//        SharedPreferencesUtils.setBoolean(
//            IS_CONFIGURATION_CHANGED,
//            true
//        )
//        activity.recreate()
//    }
//
//    fun getCurrentLocalLayoutDirection(): Int {
//        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
//            baseContext.resources.configuration.locales[0].layoutDirection
//        else baseContext.resources.configuration.locale.layoutDirection
//    }
//
//    fun isRTL(): Boolean {
//        return getCurrentLocalLayoutDirection() == View.LAYOUT_DIRECTION_RTL
//    }
//
//    fun getCurrentLocalLanguage(): String {
//        val currentLocal =
//            SharedPreferencesUtils.getString(
//                "language",
//                /*Locale.getDefault().language*/
//                "fa"
//            )
//        return currentLocal
//    }
//
//    fun getCurrentLocal(): Locale {
//        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
//            baseContext.resources.configuration.locales[0]
//        else baseContext.resources.configuration.locale
//    }
//
//    // the method is used to set the language at runtime
//    fun setLocale(language: String): Context {
//        persist(language)
//        // updating the language for devices above android nougat
//        baseContext = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            updateResources(language)
//        } else updateResourcesLegacy(language)
//        return baseContext
//        // for devices having lower version of android os
//    }
//
//    private fun persist(language: String) {
//        SharedPreferences.editSharedPreferencesString(baseContext, "language", language)
//    }
//
//    // the method is used update the language of application by creating
//    // object of inbuilt Locale class and passing language argument to it
//    @TargetApi(Build.VERSION_CODES.N)
//    private fun updateResources(language: String): Context {
//        val locale = Locale(language)
//        Locale.setDefault(locale)
//        val resources: Resources = baseContext.resources
//        val configuration: Configuration = Configuration(resources.configuration)
//        configuration.setLocale(locale)
//        configuration.setLayoutDirection(locale)
//        return baseContext.createConfigurationContext(configuration)
//    }
//
//    private fun updateResourcesLegacy(language: String): Context {
//        val locale = Locale(language)
//        Locale.setDefault(locale)
//        val resources: Resources = baseContext.resources
//        val configuration: Configuration = Configuration(resources.configuration)
//        configuration.locale = locale
//        configuration.setLayoutDirection(locale)
//        resources.updateConfiguration(configuration, resources.displayMetrics)
//        return baseContext
//    }
//
//    fun forceEnglishNumbers() {
//        Locale.setDefault(Locale.ENGLISH)
//    }
//}
//

package io.dee.note.presentation.compose.editNote.components

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.dp
import io.dee.composenewsapp.ui.theme.Black
import io.dee.composenewsapp.ui.theme.DayNightSecondary
import io.dee.composenewsapp.ui.theme.Red
import io.dee.note.R
import io.dee.note.presentation.compose.editNote.EditNoteEvents

import io.dee.note.presentation.ui.theme.deeTypography

@Composable
fun EditNoteTopBar(
    modifier: Modifier = Modifier,
    onEvents: (EditNoteEvents) -> Unit,
    isBookmark: Boolean = false,
    hasTimer: Boolean = false,
    navigateUp: () -> Unit
) {
    val context = LocalContext.current
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(10.dp)

    ) {

        Box(
            modifier = Modifier
                .background(
                    DayNightSecondary, shape = MaterialTheme.shapes.medium
                )
                .size(40.dp), contentAlignment = Alignment.Center
        ) {
            Icon(
                modifier = Modifier
                    .size(20.dp)
                    .clickable { navigateUp() },
                painter = painterResource(id = R.drawable.ic_arrow_left_linear),
                contentDescription = null,
                tint = Black
            )
        }
        Text(
            text = stringResource(id = R.string.edit_note),
            modifier = Modifier.weight(1f),
            textAlign = TextAlign.Center,
            style = deeTypography.copy(
                fontWeight = FontWeight.Bold, fontSize = TextUnit(
                    16f, TextUnitType.Sp
                )
            ),
            color = Black
        )
        Box(
            modifier = Modifier
                .background(
                    DayNightSecondary, shape = MaterialTheme.shapes.medium
                )
                .border(1.dp, shape = MaterialTheme.shapes.medium, color = Red)
                .size(40.dp), contentAlignment = Alignment.Center
        ) {
            Icon(
                modifier = Modifier
                    .size(20.dp)

                    .clickable { onEvents(EditNoteEvents.ShowDeleteDialog) },
                painter = painterResource(
                    id = R.drawable.ic_trash_linear
                ),
                contentDescription = null,
                tint = Red
            )
        }
        Box(
            modifier = Modifier
                .background(
                    DayNightSecondary, shape = MaterialTheme.shapes.medium
                )
                .size(40.dp), contentAlignment = Alignment.Center
        ) {
            Icon(
                modifier = Modifier
                    .size(20.dp)
                    .clickable { onEvents(EditNoteEvents.ShowTimerDialog) },
                painter = painterResource(
                    id = if (hasTimer) R.drawable.ic_timer_bold else R.drawable.ic_timer_linear
                ),
                contentDescription = null,
                tint = Black
            )
        }
        Box(
            modifier = Modifier
                .background(
                    DayNightSecondary, shape = MaterialTheme.shapes.medium
                )
                .size(40.dp), contentAlignment = Alignment.Center
        ) {
            Icon(
                modifier = Modifier
                    .size(20.dp)
                    .clickable { onEvents(EditNoteEvents.UpdateBookmarkState) },
                painter = painterResource(id = if (isBookmark) R.drawable.ic_bookmark_bold else R.drawable.ic_bookmark_linear),
                contentDescription = null,
                tint = Black
            )
        }
        Box(
            modifier = Modifier
                .background(
                    DayNightSecondary, shape = MaterialTheme.shapes.medium
                )
                .size(40.dp), contentAlignment = Alignment.Center
        ) {

            Icon(
                modifier = Modifier
                    .size(20.dp)
                    .clickable { onEvents(EditNoteEvents.UpdateNoteInDb(context)) },
                painter = painterResource(id = R.drawable.ic_check_linear),
                contentDescription = null,
                tint = Black
            )
        }
    }
}

@Preview(showBackground = true)
@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewEditNoteTopBar() {
    EditNoteTopBar(modifier = Modifier.fillMaxWidth(), navigateUp = {}, onEvents = {})
}
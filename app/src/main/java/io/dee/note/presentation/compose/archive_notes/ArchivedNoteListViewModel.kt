package io.dee.note.presentation.compose.archive_notes

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import dagger.hilt.android.lifecycle.HiltViewModel
import io.dee.note.data.local.database.NoteData
import io.dee.note.data.repository.note.ArchivedNoteListRepository
import io.dee.note.presentation.compose.home_screen.EditNoteScreen
import io.dee.note.presentation.compose.note_list.NoteListEvents
import io.dee.note.presentation.compose.note_list.NoteListState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ArchivedNoteListViewModel
@Inject constructor(
    private val repository: ArchivedNoteListRepository
) : ViewModel() {

    var navController: NavController? = null

    var state by mutableStateOf(NoteListState())
        private set


    init {
        getAllFavouritesNotes()
    }

    fun getAllFavouritesNotes(searchQuery: String = "") = viewModelScope.launch {
        repository.getAllArchivedNoteListAsAFlow().onEach {
            val list = if (searchQuery.isNotEmpty()) it.filter {
                it.title.contains(searchQuery, true) || it.description.contains(searchQuery, true)
            } else it
            state = state.copy(noteList = list, searchQuery = searchQuery)
        }.launchIn(viewModelScope)
    }

    fun onEvent(events: NoteListEvents) {
        when (events) {

            is NoteListEvents.SearchNote -> {
                getAllFavouritesNotes(searchQuery = events.searchQuery)
            }

            is NoteListEvents.EditNote -> {
                navController?.navigate(
                    EditNoteScreen(
                        events.note.dId
                    )
                )
            }

            is NoteListEvents.ArchiveNote -> {
                removeNoteFromArchive(note = events.note)
            }

            else -> {}
        }
    }


    fun removeNoteFromArchive(note: NoteData) = viewModelScope.launch {
        repository.upsertNote(note.apply {
            this.isArchived = false
        })
    }

}
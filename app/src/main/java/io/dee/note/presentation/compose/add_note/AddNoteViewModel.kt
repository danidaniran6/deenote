package io.dee.note.presentation.compose.add_note

import android.content.Context
import android.content.Intent
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.dee.note.data.local.database.NoteData
import io.dee.note.data.repository.note.AddNoteRepository
import io.dee.note.presentation.ui.alarm_reminder.ReminderService
import io.dee.note.presentation.ui.alarm_reminder.ReminderService.Companion.CREATE_REMINDER
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddNoteViewModel @Inject constructor(
    private val repository: AddNoteRepository
) : ViewModel() {


    var state by mutableStateOf(NoteData())
        private set

    var showSetTimerDialog by mutableStateOf<Boolean?>(null)
        private set


    fun onEvent(events: AddNoteEvents) {
        when (events) {
            is AddNoteEvents.UpdateTitleText -> {
                state = state.copy(title = events.title)
            }

            is AddNoteEvents.UpdateBodyText -> {
                state = state.copy(description = events.body)

            }

            is AddNoteEvents.UpdateBookmarkState -> {
                state = state.copy(isBookmarked = !state.isBookmarked)
            }

            is AddNoteEvents.AddNoteToDb -> {
                insertANewNote(context = events.context)
            }

            is AddNoteEvents.ShowTimerDialog -> {
                showSetTimerDialog = true
            }

            is AddNoteEvents.UpdateNoteTimer -> {
                state =
                    state.copy(reminder = events.calendar?.let {
                        NoteData.NoteReminderData(
                            dateToRemind = events.calendar
                        )
                    })
                onEvent(AddNoteEvents.RemoveTimerDialog)
            }

            is AddNoteEvents.RemoveTimerDialog -> {
                showSetTimerDialog = null
            }


        }
    }


    fun insertANewNote(context: Context) = CoroutineScope(Dispatchers.IO).launch {
        val noteData = NoteData().apply {
            this.title = state.title
            this.description = state.description
            this.createdAt = System.currentTimeMillis()
            this.isBookmarked = state.isBookmarked
        }
        if (state.reminder != null) {
            noteData.reminder = state.reminder
            val intent =
                Intent(context, ReminderService::class.java).apply { action = CREATE_REMINDER }
            intent.putExtra("note", noteData)
            context.startService(intent)
        }
        repository.insertANewNote(noteData)
    }

}
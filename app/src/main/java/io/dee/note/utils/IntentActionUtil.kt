package io.dee.note.utils

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class IntentActionUtil @Inject constructor(@ApplicationContext val context: Context) {
    companion object {
        const val OPEN_NOTE = "openNote"
        const val ADD_NOTE = "addNote"
    }


    fun generateAction(action: String): String {
        return context.packageName + action
    }
}
package io.dee.note.presentation.ui.alarm_reminder

import android.app.AlarmManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.IBinder
import android.util.Log
import io.dee.note.data.local.database.NoteData
import io.dee.note.presentation.ui.alarm_reminder.AlarmReceiver.Companion.CREATE_NEW_NOTIFICATION

class ReminderService : Service() {
    companion object {
        const val DISMISS_REMINDER = "DISMISS_REMINDER"
        const val CREATE_REMINDER = "CREATE_REMINDER"
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        when (intent?.action) {
            DISMISS_REMINDER -> {
                intent.getParcelableExtra<NoteData>("note")?.let { reminderNote ->
                    val alarmIntent = Intent(this, AlarmReceiver::class.java).apply {
                        putExtra("note", reminderNote)
                        action = CREATE_NEW_NOTIFICATION
                        data = Uri.parse(reminderNote.dId.toString())
                    }

                    val pendingIntent = PendingIntent.getBroadcast(
                        this, reminderNote.dId, alarmIntent,
                        PendingIntent.FLAG_IMMUTABLE
                    )

                    // Cancel the alarm
                    alarmManager.cancel(pendingIntent)
                }

            }

            CREATE_REMINDER -> {
                val reminderNote = intent.getParcelableExtra<NoteData>("note")
                val calendar = reminderNote?.reminder?.dateToRemind

                calendar?.let {
                    // Schedule the alarm for intended time
                    val alarmIntent = Intent(this, AlarmReceiver::class.java).apply {
                        putExtra("note", reminderNote)
                        action = CREATE_NEW_NOTIFICATION
                        data = Uri.parse(reminderNote.dId.toString())
                    }

                    val pendingIntent = PendingIntent.getBroadcast(
                        this, reminderNote.dId, alarmIntent,
                        PendingIntent.FLAG_IMMUTABLE
                    )
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                        if (alarmManager.canScheduleExactAlarms()) {
                            alarmManager.setExactAndAllowWhileIdle(
                                AlarmManager.RTC_WAKEUP,
                                calendar.timeInMillis,
                                pendingIntent
                            )
                        }
                    } else {
                        alarmManager.setExactAndAllowWhileIdle(
                            AlarmManager.RTC_WAKEUP,
                            calendar.timeInMillis,
                            pendingIntent
                        )
                    }
                    Log.d("notification", "alarm manager started")
                }
            }

            else -> {}
        }


        return START_STICKY
    }
}
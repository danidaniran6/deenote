package io.dee.note.presentation.compose.common

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TimePicker
import androidx.compose.material3.TimePickerDefaults
import androidx.compose.material3.rememberTimePickerState
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import io.dee.composenewsapp.ui.theme.Black
import io.dee.composenewsapp.ui.theme.DayNightPrimary
import io.dee.composenewsapp.ui.theme.DayNightSecondary
import java.util.Calendar

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TimePickerDialog(
    initialTime: Calendar = Calendar.getInstance().also { it.timeInMillis += 5 * 60 * 1000 },
    onTimeSelected: (selectedHour: Int?, selectedMinute: Int?) -> Unit
) {
    val timePickerState = rememberTimePickerState(
        initialHour = initialTime.get(Calendar.HOUR),
        initialMinute = initialTime.get(Calendar.MINUTE),
        is24Hour = true
    )
    AlertDialog(

        onDismissRequest = { /*TODO*/ }, confirmButton = {
            TextButton(
                onClick = {
                    onTimeSelected(timePickerState.hour, timePickerState.minute)
                }, colors = ButtonDefaults.buttonColors(
                    contentColor = Black, containerColor = DayNightSecondary
                ), contentPadding = PaddingValues(horizontal = 15.dp)
            ) { Text(text = "Apply") }
        }, text = {

            TimePicker(
                state = timePickerState,
                colors = TimePickerDefaults.colors(
                    containerColor = DayNightPrimary,
                    clockDialColor = DayNightSecondary,
                    clockDialSelectedContentColor = Color.White,
                    clockDialUnselectedContentColor = Black,
                    timeSelectorSelectedContainerColor = DayNightSecondary,
                    timeSelectorSelectedContentColor = Black,
                    timeSelectorUnselectedContainerColor = DayNightSecondary,
                    timeSelectorUnselectedContentColor = Black,
                )
            )

        }, containerColor = DayNightPrimary, dismissButton = {
            TextButton(
                onClick = {
                    onTimeSelected(null, null)
                }, colors = ButtonDefaults.buttonColors(
                    contentColor = Black, containerColor = Color.Transparent
                )
            ) { Text(text = "Dismiss") }
        })

}

@Preview
@Composable
fun PreviewTimePickerDialog() {
    TimePickerDialog(onTimeSelected = { _, _ -> })
}
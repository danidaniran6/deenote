package io.dee.note.presentation.compose.add_note.componenets

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.dp
import io.dee.composenewsapp.ui.theme.Black
import io.dee.composenewsapp.ui.theme.DayNightSecondary
import io.dee.note.R
import io.dee.note.presentation.compose.add_note.AddNoteEvents
import io.dee.note.presentation.ui.theme.deeTypography

@Composable
fun AddNoteTopBar(
    modifier: Modifier = Modifier,
    isBookmark: Boolean = false,
    hasTimer: Boolean = false,
    onEvents: (AddNoteEvents) -> Unit,
    navigateUp: () -> Unit
) {
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(10.dp)

    ) {
        Box(
            modifier = Modifier
                .background(
                    DayNightSecondary, shape = MaterialTheme.shapes.medium
                )
                .size(40.dp), contentAlignment = Alignment.Center
        ) {
            Icon(
                modifier = Modifier
                    .size(20.dp)
                    .clickable { navigateUp() },
                painter = painterResource(id = R.drawable.ic_arrow_left_linear),
                contentDescription = null,
                tint = Black
            )
        }
        Text(
            text = stringResource(id = R.string.create_note),
            modifier = Modifier.weight(1f),
            textAlign = TextAlign.Center,
            style = deeTypography.copy(
                fontWeight = FontWeight.Bold, fontSize = TextUnit(
                    16f, TextUnitType.Sp
                )
            ),
            color = Black
        )
        Box(
            modifier = Modifier
                .background(
                    DayNightSecondary, shape = MaterialTheme.shapes.medium
                )
                .size(40.dp), contentAlignment = Alignment.Center
        ) {
            Icon(
                modifier = Modifier
                    .size(20.dp)
                    .clickable { onEvents(AddNoteEvents.ShowTimerDialog) },
                painter = painterResource(
                    id = if (hasTimer) R.drawable.ic_timer_bold else R.drawable.ic_timer_linear
                ),
                contentDescription = null,
                tint = Black
            )
        }
        Box(
            modifier = Modifier
                .background(
                    DayNightSecondary, shape = MaterialTheme.shapes.medium
                )
                .size(40.dp), contentAlignment = Alignment.Center
        ) {
            Icon(
                modifier = Modifier
                    .size(20.dp)
                    .clickable { onEvents(AddNoteEvents.UpdateBookmarkState) },
                painter = painterResource(id = if (isBookmark) R.drawable.ic_bookmark_bold else R.drawable.ic_bookmark_linear),
                contentDescription = null,
                tint = Black
            )
        }
        Box(
            modifier = Modifier
                .background(
                    DayNightSecondary, shape = MaterialTheme.shapes.medium
                )
                .size(40.dp), contentAlignment = Alignment.Center
        ) {
            Icon(
                modifier = Modifier
                    .size(20.dp)
                    .clickable { navigateUp() },
                painter = painterResource(id = R.drawable.ic_check_linear),
                contentDescription = null,
                tint = Black
            )
        }
    }
}
package io.dee.note.presentation.compose.editNote

import android.content.Context
import io.dee.note.data.local.database.NoteData
import java.util.Calendar

sealed class EditNoteEvents {
    data class SetNote(val noteId : Int) : EditNoteEvents()
    data class UpdateTitleValue(val title: String) : EditNoteEvents()
    data class UpdateBodyValue(val body: String) : EditNoteEvents()
    data class UpdateNoteInDb(val context: Context) : EditNoteEvents()
    data object DeleteNote : EditNoteEvents()
    data object UpdateBookmarkState : EditNoteEvents()
    data object ShowDeleteDialog : EditNoteEvents()
    data object RemoveDeleteDialog : EditNoteEvents()
    data object ShowDiscardChangesDialog : EditNoteEvents()
    data object RemoveDiscardChangesDialog : EditNoteEvents()

    data object ShowTimerDialog : EditNoteEvents()
    data object RemoveTimerDialog : EditNoteEvents()
    data class UpdateNoteTimer(val calendar: Calendar?) : EditNoteEvents()
    data object NavigateUp : EditNoteEvents()
}

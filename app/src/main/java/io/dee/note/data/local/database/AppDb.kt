package io.dee.note.data.local.database

import android.content.Context
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import io.dee.note.R
import io.dee.note.data.local.database.daos.NoteDataDao

@Database(
    entities = [NoteData::class],
    version = 4,
    exportSchema = true,
)
@TypeConverters(NoteReminderDataTypeConverter::class)

abstract class AppDb : RoomDatabase() {
    abstract val noteDataDao: NoteDataDao

}

class MyDatabaseCallback(val context: Context) : RoomDatabase.Callback() {
    override fun onCreate(db: SupportSQLiteDatabase) {
        super.onCreate(db)
        // Insert data using SQL statements
        val noteTitle = context.getString(R.string.room_note_pre_populated_title)
        val noteDescription = context.getString(R.string.room_note_pre_populated_description)
        val favouriteNoteTitle = context.getString(R.string.room_favourite_note_pre_populated_title)
        val favouriteNoteDescription =
            context.getString(R.string.room_favourite_note_pre_populated_description)
        val ts = System.currentTimeMillis()
        db.execSQL("INSERT INTO notedata (dId, title, description,createdAt,isBookmarked,isArchived,reminder) VALUES (1, '$noteTitle', '$noteDescription','$ts','0','0','NULL')")
        db.execSQL("INSERT INTO notedata (dId, title, description,createdAt,isBookmarked,isArchived,reminder) VALUES (2, '$favouriteNoteTitle', '$favouriteNoteDescription','$ts','1','0','NULL')")
    }
}
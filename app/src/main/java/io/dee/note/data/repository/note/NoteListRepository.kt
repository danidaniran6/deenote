package io.dee.note.data.repository.note

import io.dee.note.data.local.database.NoteData
import kotlinx.coroutines.flow.Flow

interface NoteListRepository {
    suspend fun deleteTheInvalidNote()
    suspend fun upsertNote(note: NoteData)
    suspend fun getAllNotesAsAList(): List<NoteData>
    fun getAllNotesAsAFlow(): Flow<List<NoteData>>
    suspend fun getNoteById(id: Int): NoteData?

    suspend fun deleteANote(note: NoteData)
}
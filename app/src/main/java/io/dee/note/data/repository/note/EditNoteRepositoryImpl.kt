package io.dee.note.data.repository.note

import io.dee.note.data.local.database.AppDb
import io.dee.note.data.local.database.NoteData

class EditNoteRepositoryImpl(
    private val appDb: AppDb
) : EditNoteRepository {
    override suspend fun upsertNote(note: NoteData) {
        appDb.noteDataDao.upsertNote(note)
    }

    override suspend fun deleteANote(note: NoteData) {
        appDb.noteDataDao.deleteANote(note)
    }

    override suspend fun getNoteById(noteId: Int): NoteData? {
        return appDb.noteDataDao.getNoteById(id = noteId)
    }
}
package io.dee.note.presentation.compose.add_note

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import io.dee.composenewsapp.ui.theme.DayNightPrimary
import io.dee.composenewsapp.ui.theme.DayNightSecondary
import io.dee.note.data.local.database.NoteData
import io.dee.note.presentation.compose.add_note.componenets.AddNoteTopBar
import io.dee.note.presentation.compose.add_note.componenets.NoteTextField

@Composable
fun AddNoteScreenBody(
    state: NoteData, onEvent: (AddNoteEvents) -> Unit, navigateUp: () -> Unit
) {


    Scaffold(modifier = Modifier.fillMaxSize(), containerColor = DayNightPrimary, topBar = {
        AddNoteTopBar(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 15.dp, vertical = 10.dp),
            isBookmark = state.isBookmarked,
            hasTimer = state.reminder != null,
            onEvents = onEvent,
            navigateUp = navigateUp
        )
    }) { paddingValues ->
        val topPadding = paddingValues.calculateTopPadding()

        Column(
            modifier = Modifier
                .padding(top = topPadding)
                .padding(bottom = 10.dp)
                .padding(horizontal = 15.dp)
                .fillMaxSize()
                .background(color = DayNightSecondary, shape = MaterialTheme.shapes.medium)

        ) {
            NoteTextField(
                modifier = Modifier.fillMaxWidth(),
                text = state.title,
                onTextChanged = { onEvent(AddNoteEvents.UpdateTitleText(it)) },
                placeHolder = "Title"
            )
            Spacer(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(10.dp)
                    .background(color = DayNightPrimary)
            )
            NoteTextField(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f),
                text = state.description,
                onTextChanged = { onEvent(AddNoteEvents.UpdateBodyText(it)) },
                placeHolder = "Description",
                focusedByDefault = true
            )
        }

    }
}
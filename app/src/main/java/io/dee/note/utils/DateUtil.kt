package co.okex.app.utils

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.DialogInterface
import io.dee.note.R
import io.dee.note.utils.CustomExceptionHandler.handleException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.TimeZone
import java.util.regex.Pattern

object DateUtil {

    // Date format constants
    const val ISO_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    const val ISO_DATE_FORMAT_2 = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    const val DISPLAY_DATE_FORMAT = "dd/MM/yyyy"
    const val DISPLAY_DATE_FORMAT_REVERSE = "yyyy/MM/dd"
    const val DISPLAY_DATE_TIME_FORMAT = "dd/MM/yyyy,"
    const val DISPLAY_TIME_FORMAT_12_HOUR = "hh:mm a"
    const val DISPLAY_JUST_HOUR = "HH"
    const val DISPLAY_JUST_MINUTES = "mm"
    const val DISPLAY_TIME_FORMAT_24_HOUR = "HH:mm"
    const val MONTH_YEAR_FORMAT = "MMMM yyyy"
    const val MONTH_DAY_YEAR_FORMAT = "EEE dd MMM yyyy"
    const val MONTH_DAY_YEAR_FORMAT_2 = "EEE dd MMM, hh:mm a"
    const val YEAR_MONTH_FORMAT = "yyyy-MM"
    const val DAY_OF_WEEK_FORMAT = "EEEE"

    // Get current date and time in ISO format
    @SuppressLint("SimpleDateFormat")
    fun getCurrentDateTimeInIsoFormat(): String {
        val sdf = SimpleDateFormat(ISO_DATE_FORMAT)
        sdf.timeZone = TimeZone.getDefault()
        return sdf.format(Date())
    }

    @SuppressLint("SimpleDateFormat")
    fun getCurrentDateTime(format: String): String {
        val sdf = SimpleDateFormat(format)
        sdf.timeZone = TimeZone.getDefault()
        return sdf.format(Date())
    }

    // Convert ISO date string to Date object
    @SuppressLint("SimpleDateFormat")
    fun parseIsoDateString(isoDate: String): Date {
        val sdf = SimpleDateFormat(ISO_DATE_FORMAT)
        sdf.timeZone = TimeZone.getDefault()
        return sdf.parse(isoDate)!!
    }

    // Convert timestamp to Date object
    fun convertTimestampToDate(timestamp: Long): Date {
        return Date(timestamp)
    }

    // Convert Date object to display format string
    @SuppressLint("SimpleDateFormat")
    fun formatDisplayDate(date: Date): String {

        val sdf = SimpleDateFormat(DISPLAY_DATE_FORMAT)
        return sdf.format(date)
    }


    // Convert Date object to display format string with time
    @SuppressLint("SimpleDateFormat")
    fun formatDisplayDateTime(
        date: Date,
        delimiter: String = ",",
        is24HourFormat: Boolean = false,
        showSeconds: Boolean = false
    ): String {
        val sdf = SimpleDateFormat(DISPLAY_DATE_TIME_FORMAT)


        if (is24HourFormat) {
            sdf.applyPattern(DISPLAY_DATE_TIME_FORMAT + DISPLAY_TIME_FORMAT_24_HOUR)
        } else {
            sdf.applyPattern(DISPLAY_DATE_TIME_FORMAT + DISPLAY_TIME_FORMAT_12_HOUR)
        }
        if (showSeconds) {
            sdf.applyPattern("${sdf.toPattern()}:ss")
        }
        return sdf.format(date).replace(",", delimiter)
    }


    @SuppressLint("SimpleDateFormat")
    fun formatDisplayHour(
        date: Date
    ): String? {
        val sdf = SimpleDateFormat(DISPLAY_JUST_HOUR)
        return try {
            sdf.format(date)
        } catch (e: Exception) {
            e.handleException()
            null
        }
    }

    fun formatDisplayMinutes(
        date: Date
    ): String? {
        val sdf = SimpleDateFormat(DISPLAY_JUST_MINUTES)
        return try {
            sdf.format(date)
        } catch (e: Exception) {
            e.handleException()
            null
        }
    }

    // Convert Date object to display time string
    @SuppressLint("SimpleDateFormat")
    fun formatDisplayTime(
        date: Date,
        is24HourFormat: Boolean = false,
        showSeconds: Boolean = false
    ): String {
        val sdf = SimpleDateFormat(
            if (is24HourFormat) DISPLAY_TIME_FORMAT_24_HOUR else DISPLAY_TIME_FORMAT_12_HOUR
        )
        if (showSeconds) {
            sdf.applyPattern("${sdf.toPattern()}:ss")
        }
        return sdf.format(date)
    }


    // Convert Date object to month and year format string
    @SuppressLint("SimpleDateFormat")
    fun formatMonthYear(date: Date): String {
        val sdf = SimpleDateFormat(MONTH_YEAR_FORMAT)
        return sdf.format(date)
    }

    // Convert Date object to year and month format string
    @SuppressLint("SimpleDateFormat")
    fun formatYearMonth(date: Date): String {
        val sdf = SimpleDateFormat(YEAR_MONTH_FORMAT)
        return sdf.format(date)
    }

    // Convert Date object to day of week format string
    @SuppressLint("SimpleDateFormat")
    fun formatDayOfWeek(date: Date): String {
        val sdf = SimpleDateFormat(DAY_OF_WEEK_FORMAT)
        return sdf.format(date)
    }


    // Convert string to Date object
    @SuppressLint("SimpleDateFormat")
    fun parseDateString(dateString: String, dateFormat: String = ISO_DATE_FORMAT): Date? {
        return try {
            val sdf = SimpleDateFormat(dateFormat)
            sdf.timeZone = TimeZone.getTimeZone("GMT")
            sdf.parse(dateString)
        } catch (e: Exception) {
            val sdf = SimpleDateFormat(ISO_DATE_FORMAT_2)
            sdf.timeZone = TimeZone.getTimeZone("GMT")
            sdf.parse(dateString)
        } catch (e: Exception) {
            null
        }
    }

    fun formatDateToCustomFormat(
        date: Date,
        outputFormat: String = "MMM d'${getDaySuffix(date)}'"
    ): String {
        return formatCustomDate(date, outputFormat)
    }

    // Function to get the day suffix (st, nd, rd, or th)
    private fun getDaySuffix(date: Date): String {
        val dayOfMonth = SimpleDateFormat("d").format(date).toInt()
        return when (dayOfMonth) {
            1, 21, 31 -> "st"
            2, 22 -> "nd"
            3, 23 -> "rd"
            else -> "th"
        }
    }

    // Function to format Date object to custom format
    private fun formatCustomDate(date: Date, outputFormat: String): String {
        val sdf = SimpleDateFormat(outputFormat)
        return sdf.format(date)
    }

    @SuppressLint("SimpleDateFormat")
    fun removeSeconds(timeString: String?): String {
        if (timeString.isNullOrEmpty()) return ""
        val inputFormat = SimpleDateFormat("HH:mm:ss")
        val outputFormat = SimpleDateFormat("HH:mm")

        val date = inputFormat.parse(timeString)
        return date?.let { outputFormat.format(it) }?:""
    }

    fun showDatePicker(
        context: Context,
        prevCal: Calendar? = null,
        onDateSelectedMillis: (calYear: Int, calMonth: Int, calDayOfMonth: Int) -> Unit
    ) {
        val myCurrentTime: Calendar = prevCal ?: Calendar.getInstance()
        val year: Int = myCurrentTime.get(Calendar.YEAR)
        val month: Int = myCurrentTime.get(Calendar.MONTH)
        val dayOfMonth: Int = myCurrentTime.get(Calendar.DAY_OF_MONTH)
        val dataPickerDialog = DatePickerDialog(
            context,
            { _, calYear, calMonth, calDayOfMonth ->
                onDateSelectedMillis.invoke(calYear, calMonth, calDayOfMonth)

            },
            year,
            month,
            dayOfMonth
        )
        dataPickerDialog.datePicker.minDate = System.currentTimeMillis()
        dataPickerDialog.show()


    }


}

fun showTimePicker(
    context: Context,
    prevCal: Calendar? = null,
    onTimeSelected: (Hour: Int, Minute: Int) -> Unit
) {
    val c = prevCal ?: Calendar.getInstance()
    val currentHour = c.get(Calendar.HOUR_OF_DAY)
    val currentMinute = c.get(Calendar.MINUTE)

    val timeDialog = TimePickerDialog(
        context, { _, hourOfDay, minute ->
            // selected time

            onTimeSelected.invoke(hourOfDay, minute)
        }, currentHour, currentMinute, true
    )

    timeDialog.show()
    timeDialog.getButton(DialogInterface.BUTTON_POSITIVE)?.apply {
//        setTextColor(
//            ContextCompat.getColor(
//                context, R.color.secondary
//            )
//        )
        setText(context.getString(R.string.submit))

    }
    timeDialog.getButton(DialogInterface.BUTTON_NEGATIVE)?.apply {
//
//        setTextColor(
//            ContextCompat.getColor(
//                context, R.color.secondary
//            )
//        )
        setText(R.string.dismiss)
    }
    timeDialog.getButton(DialogInterface.BUTTON_NEUTRAL)?.apply {
//        setTextColor(
//            ContextCompat.getColor(
//                context, R.color.secondary
//            )
//        )
        setText(R.string.dismiss)

    }


}


fun Date.formatDisplayDateTime(
    delimiter: String = ",",
    is24HourFormat: Boolean = false,
    showSeconds: Boolean = false
): String {
    val sdf =
        SimpleDateFormat(DateUtil.DISPLAY_DATE_TIME_FORMAT)

    if (is24HourFormat) {
        sdf.applyPattern(DateUtil.DISPLAY_DATE_TIME_FORMAT + DateUtil.DISPLAY_TIME_FORMAT_24_HOUR)
    } else {
        sdf.applyPattern(DateUtil.DISPLAY_DATE_TIME_FORMAT + DateUtil.DISPLAY_TIME_FORMAT_12_HOUR)
    }
    if (showSeconds) {
        sdf.applyPattern("${sdf.toPattern()}:ss")
    }
    return sdf.format(this).replace(",", delimiter)
}

fun Date.formatDisplayDateTime2(
    delimiter: String = ",",
    is24HourFormat: Boolean = false,
    showSeconds: Boolean = false
): String {
    val sdf =
        SimpleDateFormat(DateUtil.DISPLAY_DATE_TIME_FORMAT)

    if (is24HourFormat) {
        sdf.applyPattern(DateUtil.MONTH_DAY_YEAR_FORMAT + DateUtil.DISPLAY_TIME_FORMAT_24_HOUR)
    } else {
        sdf.applyPattern(DateUtil.MONTH_DAY_YEAR_FORMAT + DateUtil.DISPLAY_TIME_FORMAT_12_HOUR)
    }
    if (showSeconds) {
        sdf.applyPattern("${sdf.toPattern()}:ss")
    }
    return sdf.format(this).replace(",", delimiter)
}


fun Date.formatDisplayDate(): String {
    val sdf = SimpleDateFormat(DateUtil.DISPLAY_DATE_FORMAT)
    return sdf.format(this)
}

fun Date.formatDisplayNameDate(pattern: String = DateUtil.MONTH_DAY_YEAR_FORMAT): String {
    val sdf = SimpleDateFormat(pattern)
    return sdf.format(this)
}

fun Date.formatDisplayTime(
    is24HourFormat: Boolean = false,
    showSeconds: Boolean = false
): String {
    val sdf = SimpleDateFormat(
        if (is24HourFormat) DateUtil.DISPLAY_TIME_FORMAT_24_HOUR else DateUtil.DISPLAY_TIME_FORMAT_12_HOUR
    )
    if (showSeconds) {
        sdf.applyPattern("${sdf.toPattern()}:ss")
    }
    return sdf.format(this)
}

fun Date.justHour(): Int? {
    return try {
        val sdf = SimpleDateFormat(
            DateUtil.DISPLAY_JUST_HOUR
        )
        sdf.format(this).toIntOrNull()
    } catch (e: Exception) {
        -1
    }
}

fun Date.justMinute(): Int? {
    return try {
        val sdf = SimpleDateFormat(
            DateUtil.DISPLAY_JUST_MINUTES
        )
        sdf.format(this).toIntOrNull()
    } catch (e: Exception) {
        -1
    }
}



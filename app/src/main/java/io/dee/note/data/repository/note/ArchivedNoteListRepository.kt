package io.dee.note.data.repository.note

import io.dee.note.data.local.database.NoteData
import kotlinx.coroutines.flow.Flow

interface ArchivedNoteListRepository {
    suspend fun upsertNote(note: NoteData)
    suspend fun getAllArchivedNoteListAsAList(): List<NoteData>
    fun getAllArchivedNoteListAsAFlow(): Flow<List<NoteData>>
}
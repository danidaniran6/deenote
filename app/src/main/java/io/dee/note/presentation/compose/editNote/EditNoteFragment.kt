package io.dee.note.presentation.compose.editNote

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import io.dee.note.R
import io.dee.note.presentation.compose.common.NormalAlertDialog
import io.dee.note.presentation.compose.common.SetTimerAlertDialog

@Composable
fun EditNoteFragment(
    viewModel: EditNoteViewModel,
    navigateUp : ()->Unit
) {
    BackHandler(true) {
        if (viewModel.isNoteEdited()) {
            viewModel.onEvent(EditNoteEvents.ShowDiscardChangesDialog)
        } else {
            navigateUp()
        }
    }

    viewModel.shouldNavigateUp?.let {
        navigateUp()
    }
    viewModel.showDeleteDialog?.let {
        if (it) {
            NormalAlertDialog(
                onDismissRequest = {
                    viewModel.onEvent(EditNoteEvents.RemoveDeleteDialog)
                },
                onConfirmation = {
                    viewModel.onEvent(
                        EditNoteEvents.DeleteNote
                    )
                },
                dialogTitle = stringResource(id = R.string.warning),
                dialogText = stringResource(id = R.string.you_are_about_to_delete_note_are_you_sure)
            )
        }
    }

    viewModel.showSetTimerDialog?.let {
        SetTimerAlertDialog(reminderTime = viewModel.state.reminder?.dateToRemind,
            onDateSelected = { calendar ->
                viewModel.onEvent(EditNoteEvents.UpdateNoteTimer(calendar))
            })
    }

    viewModel.showDiscardChangesDialog?.let {
        NormalAlertDialog(
            onDismissRequest = {
                viewModel.onEvent(EditNoteEvents.RemoveDiscardChangesDialog)

            },
            onConfirmation = {
                navigateUp()
            },
            dialogTitle = stringResource(id = R.string.warning),
            dialogText = stringResource(id = R.string.discarding_changes),
            confirmButtonText = stringResource(id = R.string.discard),
            dismissButtonText = stringResource(id = R.string.dismiss)
        )
    }

    EditNoteScreen(viewModel.state, onEvent = viewModel::onEvent, navigateUp = {
        navigateUp()
    })
}
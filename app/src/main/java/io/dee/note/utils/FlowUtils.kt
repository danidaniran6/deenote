package io.dee.note.utils

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


fun <T> Fragment.flowCollect(flow: Flow<T>, collect: (T) -> Unit) {
    this.viewLifecycleOwner.lifecycleScope.launch {
        repeatOnLifecycle(Lifecycle.State.CREATED) {
            flow.collect {
                collect.invoke(it)
            }
        }
    }
}

fun <T> Fragment.flowCollectLatest(flow: Flow<T>, collect: (T) -> Unit) {
    this.viewLifecycleOwner.lifecycleScope.launch {
        repeatOnLifecycle(Lifecycle.State.CREATED) {
            flow.collectLatest {
                collect.invoke(it)
            }
        }
    }
}

    val Context.preferencesDataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")

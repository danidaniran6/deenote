package io.dee.note.data.repository.note

import io.dee.note.data.local.database.AppDb
import io.dee.note.data.local.database.NoteData

class AddNoteRepositoryImpl(
    private val appDb: AppDb
) : AddNoteRepository {
    override suspend fun insertANewNote(note: NoteData) {
        appDb.noteDataDao.insertANewNote(note)
    }
}
package io.dee.note.presentation.ui.theme

import androidx.compose.material3.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import io.dee.composenewsapp.ui.theme.Black


// Set of Material typography styles to start with
// body is "Text" in the Figma Design
// label small is xsmall in Figma Design

val deeTypography = TextStyle(
    fontSize = TextUnit(14f, TextUnitType.Sp),
    letterSpacing = TextUnit(0.25f, TextUnitType.Em),

)
val Typography = Typography(
    titleMedium = TextStyle(
        fontSize = TextUnit(16f, TextUnitType.Sp), fontWeight = FontWeight.Bold
    ), bodyMedium = TextStyle(
        fontSize = TextUnit(14f, TextUnitType.Sp)
    ), labelMedium = TextStyle(
        fontSize = TextUnit(10f, TextUnitType.Sp)
    )
)
package io.dee.note.presentation.compose.common

import android.content.res.Configuration
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import co.okex.app.utils.DateUtil
import co.okex.app.utils.formatDisplayNameDate
import com.skydoves.balloon.BalloonAnimation
import com.skydoves.balloon.BalloonSizeSpec
import com.skydoves.balloon.compose.Balloon
import com.skydoves.balloon.compose.BalloonWindow
import com.skydoves.balloon.compose.rememberBalloonBuilder
import io.dee.composenewsapp.ui.theme.Black
import io.dee.composenewsapp.ui.theme.DayNightPrimary
import io.dee.composenewsapp.ui.theme.DayNightSecondary
import io.dee.note.R
import io.dee.note.data.local.database.NoteData
import io.dee.note.presentation.compose.note_list.NoteListEvents
import java.util.Calendar
import java.util.Date

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun NoteItem(
    modifier: Modifier = Modifier,
    note: NoteData,
    navigateToNoteDetails: () -> Unit,
    showMoreOption: (NoteListEvents) -> Unit,
    onBookmarkClicked: () -> Unit
) {
    val context = LocalContext.current
    val builder = rememberBalloonBuilder {
        setIsVisibleArrow(false)
        setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent))
        setWidth(BalloonSizeSpec.WRAP)
        setHeight(BalloonSizeSpec.WRAP)
        setBalloonAnimation(BalloonAnimation.FADE)
    }
    var balloonWindow: BalloonWindow? by remember { mutableStateOf(null) }

    Balloon(
        modifier = Modifier,
        builder = builder,
        balloonContent = {
            NoteBalloon(note, onEvent = {
                balloonWindow?.dismiss()
                showMoreOption(it)
            })
        }, onBalloonWindowInitialized = { balloonWindow = it }
    ) {
        Card(
            modifier = Modifier
                .fillMaxWidth()
                .combinedClickable(
                    onClick = { navigateToNoteDetails() },
                    onLongClick = {
                        balloonWindow?.showAlignBottom(yOff = -120)
                    }
                )
                .then(modifier),
            colors = CardDefaults.cardColors(
                containerColor = DayNightSecondary,
            )
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(15.dp)
            ) {
                if (note.title.isNotEmpty())
                    Text(
                        text = note.title,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(bottom = 10.dp),
                        style = MaterialTheme.typography.titleMedium.copy(color = Black)
                    )
                if (note.description.isNotEmpty())
                    Text(
                        text = note.description,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(bottom = 10.dp),
                        style = MaterialTheme.typography.bodyMedium.copy(
                            color = Black
                        ),
                        maxLines = 4,
                        overflow = TextOverflow.Ellipsis,
                        textAlign = TextAlign.Start
                    )

                if (note.reminder?.dateToRemind != null) {
                    val isTimerActive =
                        note.reminder?.dateToRemind!!.timeInMillis >= System.currentTimeMillis()
                    Card(
                        onClick = {},
                        modifier = Modifier.padding(bottom = 10.dp),
                        colors = CardDefaults.cardColors(
                            containerColor = DayNightPrimary,
                            disabledContainerColor = DayNightPrimary.copy(0.3f),
                            contentColor = Black,
                            disabledContentColor = Black.copy(0.3f)
                        ),
                        enabled = isTimerActive,
                    ) {
                        Row(
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Icon(
                                modifier = Modifier
                                    .padding(horizontal = 10.dp)
                                    .size(18.dp),
                                painter = painterResource(id = R.drawable.ic_timer_linear),
                                contentDescription = null
                            )
                            Text(
                                modifier = Modifier
                                    .padding(vertical = 10.dp)
                                    .padding(end = 10.dp),
                                text = note.reminder?.dateToRemind?.time?.formatDisplayNameDate(
                                    DateUtil.MONTH_DAY_YEAR_FORMAT_2
                                )
                                    ?: "", style = MaterialTheme.typography.labelMedium.copy(
                                ),
                                textDecoration = if (!isTimerActive) TextDecoration.LineThrough else TextDecoration.None

                            )
                        }
                    }
                }




                Row(
                    Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.Center,
                    verticalAlignment = Alignment.Bottom
                ) {
                    Text(
                        text = Date(note.createdAt).formatDisplayNameDate(),
                        modifier = Modifier
                            .fillMaxWidth()
                            .weight(1f)
                            .padding(top = 10.dp),
                        style = MaterialTheme.typography.labelMedium.copy(
                            color = Black
                        )
                    )
                    if (!note.isArchived)
                        Icon(
                            modifier = Modifier
                                .size(20.dp)
                                .clickable { onBookmarkClicked() },
                            painter = painterResource(id = if (note.isBookmarked) R.drawable.ic_bookmark_bold else R.drawable.ic_bookmark_linear),
                            contentDescription = null,
                            tint = Black
                        )
                }

            }
        }
    }


}

@Preview(showBackground = true)
@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewNoteItem() {
    NoteItem(
        note = NoteData(
            title = "title",
            description = "description",
            reminder = NoteData.NoteReminderData(
                dateToRemind = Calendar.getInstance().also { it.timeInMillis += 5 * 60 * 1000 })
        ),
        navigateToNoteDetails = {},
        onBookmarkClicked = {},
        showMoreOption = {})
}
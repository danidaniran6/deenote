package io.dee.note.presentation.compose.home_screen


sealed class DrawerEvents {
    data object ChangeTheme : DrawerEvents()
    data object NavigateToArchive : DrawerEvents()
}

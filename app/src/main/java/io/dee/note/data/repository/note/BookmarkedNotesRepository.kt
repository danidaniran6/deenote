package io.dee.note.data.repository.note

import io.dee.note.data.local.database.NoteData
import kotlinx.coroutines.flow.Flow

interface BookmarkedNotesRepository {
    suspend fun upsertNote(note: NoteData)
    suspend fun deleteANote(note: NoteData)
    suspend fun getAllBookmarkedNotesAsAList(): List<NoteData>
    fun getAllBookmarkedNotesAsAFlow(): Flow<List<NoteData>>
}
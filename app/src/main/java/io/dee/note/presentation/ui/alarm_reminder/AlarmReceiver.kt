package io.dee.note.presentation.ui.alarm_reminder

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.core.app.NotificationCompat
import dagger.hilt.android.AndroidEntryPoint
import io.dee.note.R
import io.dee.note.app.REMINDER_NOTIFICATION_CHANNEL
import io.dee.note.data.local.database.NoteData
import io.dee.note.presentation.ui.activity.main.MainActivity
import io.dee.note.utils.IntentActionUtil
import javax.inject.Inject

@AndroidEntryPoint
class AlarmReceiver : BroadcastReceiver() {
    @Inject
    lateinit var actionUtil: IntentActionUtil

    companion object {
        const val DISMISS_NOTIFICATION = "DISMISS_NOTIFICATION"
        const val CREATE_NEW_NOTIFICATION = "CREATE_NEW_NOTIFICATION"
        const val NOTIFICATION_ID = "NOTIFICATION_ID"
    }

    override fun onReceive(context: Context, intent: Intent) {

        when (intent.action) {
            DISMISS_NOTIFICATION -> {
                val notificationId = intent.getIntExtra(NOTIFICATION_ID, -1)
                if (notificationId == -1) return
                val notificationManager =
                    context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.cancel(notificationId)

            }

            CREATE_NEW_NOTIFICATION -> {
                val reminderNote = intent.getParcelableExtra<NoteData>("note")
                reminderNote?.let {
                    val notificationManager =
                        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    val openAppIntent = Intent(context, MainActivity::class.java).apply {
                        putExtra(
                            MainActivity.WidgetAction,
                            actionUtil.generateAction(IntentActionUtil.OPEN_NOTE)
                        )
                        putExtra(MainActivity.KEY_DESTINATION, reminderNote.dId)
                        data = Uri.parse(toUri(Intent.URI_INTENT_SCHEME))
                        flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    }
                    val pendingIntentOpenApp = PendingIntent.getActivity(
                        context, 0, openAppIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
                    )
                    val intentDismissNotification =
                        Intent(context, AlarmReceiver::class.java).apply {
                            action = DISMISS_NOTIFICATION
                            putExtra(NOTIFICATION_ID, reminderNote.dId)
                        }
                    val pendingIntentDismissNotification = PendingIntent.getBroadcast(
                        context, 0, intentDismissNotification,
                        PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
                    )
                    val builder = NotificationCompat.Builder(context, REMINDER_NOTIFICATION_CHANNEL)
                        .setContentTitle(it.title)
                        .setContentText(it.description)
                        .setSmallIcon(R.drawable.ic_add_note_linear)
                        .setOngoing(true) // Set this flag to make the notification sticky
                        .setShowWhen(true)
                        .setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_VIBRATE)
                        .setContentIntent(pendingIntentOpenApp)
                        .addAction(
                            R.drawable.ic_add_note_linear,
                            context.getString(R.string.dismiss),
                            pendingIntentDismissNotification
                        )

                    val notification = builder.build()
                    notificationManager.notify(reminderNote.dId, notification)
                    Log.d("notification", "notification posted")
                }
            }
        }


    }
}


package io.dee.note.presentation.compose.home_screen.components

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.size
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.NavigationBarItemDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import io.dee.composenewsapp.ui.theme.Black
import io.dee.composenewsapp.ui.theme.DayNightPrimary
import io.dee.composenewsapp.ui.theme.DayNightSecondary
import io.dee.note.presentation.compose.home_screen.BottomAppBarAction
import io.dee.note.presentation.compose.home_screen.BottomBarItems

@Composable
fun HomeBottomAppBar(
    modifier: Modifier = Modifier,
    list: List<BottomBarItems?>,
    selectedItem: String,
    onItemClicked: (BottomAppBarAction) -> Unit
) {
    BottomAppBar(
        modifier = modifier,
        containerColor = DayNightSecondary,
    ) {
        list.forEach { item ->
            NavigationBarItem(
                selected = selectedItem == item?.action?.action,
                onClick = {
                    item?.action?.let { onItemClicked(it) }

                },
                icon = {
                    if (item != null)
                        Icon(
                            modifier = Modifier.size(25.dp),
                            painter = painterResource(id = item.icon),
                            contentDescription = null
                        ) else Spacer(modifier = Modifier.size(25.dp))
                },
                enabled = item != null,
                colors = NavigationBarItemDefaults.colors(
                    selectedIconColor = Black,
                    selectedTextColor = Black,
                    unselectedTextColor = Black,
                    unselectedIconColor = Black,
                    indicatorColor = DayNightPrimary
                )
            )
        }
    }

}
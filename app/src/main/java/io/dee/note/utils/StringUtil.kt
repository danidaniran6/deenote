package io.dee.note.utils

object StringUtil {
    fun make2Digits(str: String?): String {
        if (str.isNullOrEmpty()) return ""
        var res = str
        if (str.length == 1)
            res = "0${str}"

        return res
    }

}

fun String?.make2Digits(): String {
    if (this.isNullOrEmpty()) return ""
    var res = this
    if (this.length == 1)
        res = "0${this}"

    return res
}
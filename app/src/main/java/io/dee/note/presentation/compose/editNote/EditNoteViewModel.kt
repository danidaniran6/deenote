package io.dee.note.presentation.compose.editNote

import android.content.Context
import android.content.Intent
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import io.dee.note.data.local.database.NoteData
import io.dee.note.data.repository.note.EditNoteRepository
import io.dee.note.presentation.ui.alarm_reminder.ReminderService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class EditNoteViewModel @Inject constructor(
    private val repository: EditNoteRepository
) : ViewModel() {
    var state by mutableStateOf(NoteData())
        private set

    var prevNote: NoteData = NoteData()
    var showSetTimerDialog by mutableStateOf<Boolean?>(null)
        private set

    var showDeleteDialog by mutableStateOf<Boolean?>(null)
        private set

    var showDiscardChangesDialog by mutableStateOf<Boolean?>(null)
        private set

    var shouldNavigateUp by mutableStateOf<Boolean?>(null)
        private set

    init {

    }

    fun onEvent(events: EditNoteEvents) {
        when (events) {
            is EditNoteEvents.SetNote -> {
                if (prevNote == NoteData())
                    getNoteFromDb(events.noteId)

            }

            is EditNoteEvents.UpdateTitleValue -> {
                if (events.title != state.title)
                    state = state.copy(title = events.title)
            }

            is EditNoteEvents.UpdateBodyValue -> {
                if (events.body != state.description)
                    state = state.copy(description = events.body)
            }

            is EditNoteEvents.UpdateNoteInDb -> {
                updateNote(events.context)
            }

            is EditNoteEvents.ShowDeleteDialog -> {
                showDeleteDialog = true
            }

            is EditNoteEvents.RemoveDeleteDialog -> {
                showDeleteDialog = null
            }

            is EditNoteEvents.DeleteNote -> {
                deleteNote(prevNote)
            }


            is EditNoteEvents.ShowTimerDialog -> {
                showSetTimerDialog = true
            }

            is EditNoteEvents.UpdateNoteTimer -> {
                state =
                    state.copy(reminder = events.calendar?.let {
                        NoteData.NoteReminderData(
                            dateToRemind = events.calendar
                        )
                    })
                onEvent(EditNoteEvents.RemoveTimerDialog)
            }

            is EditNoteEvents.RemoveTimerDialog -> {
                showSetTimerDialog = null
            }

            is EditNoteEvents.UpdateBookmarkState -> {
                state = state.copy(isBookmarked = !state.isBookmarked)
            }

            is EditNoteEvents.ShowDiscardChangesDialog -> {
                showDiscardChangesDialog = true
            }

            is EditNoteEvents.RemoveDiscardChangesDialog -> {
                showDiscardChangesDialog = null
            }

            is EditNoteEvents.NavigateUp -> {
                shouldNavigateUp = true
            }


        }
    }

    private fun getNoteFromDb(noteId: Int) = viewModelScope.launch {
        val note = repository.getNoteById(noteId)
        note?.let {
            state = note.copy()
            prevNote = note.copy()
        }

    }


    private fun deleteNote(note: NoteData) = viewModelScope.launch {
        repository.deleteANote(note)
        delay(400)
        onEvent(EditNoteEvents.NavigateUp)
    }

    fun isNoteEdited() =
        !(state.title == prevNote.title && state.description == prevNote.description && prevNote.isBookmarked == state.isBookmarked && prevNote.reminder == state.reminder)

    private fun updateNote(context: Context) =
        CoroutineScope(Dispatchers.IO).launch {
            if (!isNoteEdited()) {
                shouldNavigateUp = true
                return@launch
            }
            val newNote = prevNote.copy()
            newNote.apply {
                this.title = state.title
                this.description = state.description
                this.isBookmarked = state.isBookmarked
            }
            if (prevNote.reminder == null && state.reminder != null) {
                // add reminder
                newNote.reminder = state.reminder
                val intent = Intent(context, ReminderService::class.java).apply {
                    action =
                        ReminderService.CREATE_REMINDER
                    putExtra("note", newNote)
                }

                context.startService(intent)
            } else if (prevNote.reminder != null && state.reminder != null) {
                // edit reminder
                val dismissReminderIntent = Intent(context, ReminderService::class.java).apply {
                    action =
                        ReminderService.DISMISS_REMINDER
                    putExtra("note", prevNote)
                }
                context.startService(dismissReminderIntent)

                newNote.reminder = state.reminder
                val intent = Intent(context, ReminderService::class.java).apply {
                    action =
                        ReminderService.CREATE_REMINDER
                }
                intent.putExtra("note", newNote)
                context.startService(intent)
            } else if (prevNote.reminder != null && state.reminder == null) {
                //delete reminder
                newNote.reminder = null
                val dismissReminderIntent = Intent(context, ReminderService::class.java).apply {
                    action =
                        ReminderService.DISMISS_REMINDER
                    putExtra("note", prevNote)
                }
                context.startService(dismissReminderIntent)
            }
            repository.upsertNote(newNote)
            onEvent(EditNoteEvents.NavigateUp)

        }

}
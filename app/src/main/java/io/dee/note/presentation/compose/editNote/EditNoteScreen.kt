package io.dee.note.presentation.compose.editNote

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import io.dee.composenewsapp.ui.theme.DayNightPrimary
import io.dee.composenewsapp.ui.theme.DayNightSecondary
import io.dee.note.data.local.database.NoteData
import io.dee.note.presentation.compose.add_note.componenets.NoteTextField
import io.dee.note.presentation.compose.editNote.components.EditNoteTopBar

@Composable
fun EditNoteScreen(
    state: NoteData,
    onEvent: (EditNoteEvents) -> Unit,
    navigateUp: () -> Unit
) {
    Scaffold(modifier = Modifier
        .fillMaxSize()
        .statusBarsPadding(),
        topBar = {
            EditNoteTopBar(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 15.dp, vertical = 10.dp),
                navigateUp = navigateUp,
                onEvents = onEvent,
                isBookmark = state.isBookmarked,
                hasTimer = state.reminder != null
            )
        }) { paddingValues ->
        val topPadding = paddingValues.calculateTopPadding()
        var titleTextFieldValue by remember {
            mutableStateOf(TextFieldValue())
        }
        titleTextFieldValue = TextFieldValue(state.title, TextRange(state.title.length))

        Column(
            modifier = Modifier
                .padding(top = topPadding)
                .padding(bottom = 10.dp)
                .padding(horizontal = 15.dp)
                .fillMaxSize()
                .background(color = DayNightSecondary, shape = MaterialTheme.shapes.medium)

        ) {
            NoteTextField(
                modifier = Modifier.fillMaxWidth(),
                text = titleTextFieldValue,
                onTextChanged = {
                    titleTextFieldValue = it
                    onEvent(EditNoteEvents.UpdateTitleValue(it.text))
                },
                placeHolder = "Title"
            )
            Spacer(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(10.dp)
                    .background(color = DayNightPrimary)
            )
            var descriptionTextFieldValue by remember(key1 = state.description) {
                mutableStateOf(
                    TextFieldValue(
                        state.description,
                        TextRange(state.description.length)
                    )
                )
            }
            NoteTextField(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f),
                text = descriptionTextFieldValue,
                onTextChanged = {
                    descriptionTextFieldValue = it
                    onEvent(EditNoteEvents.UpdateBodyValue(it.text))
                },
                placeHolder = "Description",
                focusedByDefault = true
            )
        }
    }
}

@Preview
@Composable
fun PreviewEditNoteScreen() {
    EditNoteScreen(
        state = NoteData(isBookmarked = true),
        onEvent = {},
        navigateUp = {}
    )
}
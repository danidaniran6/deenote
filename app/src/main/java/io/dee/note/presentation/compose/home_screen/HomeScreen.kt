package io.dee.note.presentation.compose.home_screen

import android.content.res.Configuration
import android.widget.Toast
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.PagerState
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.material3.DrawerState
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.Scaffold
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.glance.appwidget.GlanceAppWidgetManager
import androidx.glance.appwidget.state.updateAppWidgetState
import androidx.glance.state.PreferencesGlanceStateDefinition
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.google.gson.Gson
import io.dee.composenewsapp.ui.theme.Black
import io.dee.composenewsapp.ui.theme.DayNightPrimary
import io.dee.composenewsapp.ui.theme.DayNightSecondary
import io.dee.note.R
import io.dee.note.presentation.compose.bookmark_list.BookmarkedNoteList
import io.dee.note.presentation.compose.bookmark_list.BookmarkedNoteListViewModel
import io.dee.note.presentation.compose.common.NormalAlertDialog
import io.dee.note.presentation.compose.home_screen.components.DrawerContent
import io.dee.note.presentation.compose.home_screen.components.HomeBottomAppBar
import io.dee.note.presentation.compose.note_list.NoteListEvents
import io.dee.note.presentation.compose.note_list.NoteListScreen
import io.dee.note.presentation.compose.note_list.NoteListViewModel
import io.dee.note.presentation.ui.widget.MyAppWidget
import kotlinx.coroutines.launch
import kotlinx.serialization.Serializable


@Serializable
object HomeScreen

@Serializable
object AddNoteScreen

@Serializable
object ArchivedNoteScreen

@Serializable
data class EditNoteScreen(
    val noteId: Int
)


@Composable
fun HomeScreen(
    modifier: Modifier = Modifier,
    navController: NavController,
    changeTheme: () -> Unit
) {
    val context = LocalContext.current
    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    val coroutineScope = rememberCoroutineScope()
    ModalNavigationDrawer(
        modifier = modifier,
        drawerState = drawerState,
        drawerContent = {
            ModalDrawerSheet(
                modifier = Modifier.fillMaxWidth(0.7f),
                drawerContainerColor = DayNightPrimary,
                drawerTonalElevation = 10.dp
            ) {

                DrawerContent(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(15.dp),
                    onEvent = { drawerEvents ->
                        coroutineScope.launch {
                            drawerState.close()
                        }
                        when (drawerEvents) {
                            is DrawerEvents.ChangeTheme -> {
                                changeTheme()
                            }

                            is DrawerEvents.NavigateToArchive -> {
                                navController.navigate(ArchivedNoteScreen)
                            }
                        }
                    }
                )
            }
        }, gesturesEnabled = !drawerState.isClosed
    ) {
        val pagerState = rememberPagerState(initialPage = 0) {
            2
        }

        var selectedItem by remember(key1 = pagerState) {
            mutableStateOf(
                if (pagerState.currentPage == 0)
                    BottomAppBarAction.NoteList.action else BottomAppBarAction.BookmarkedNoteList.action
            )
        }
        val bottomBarItems = remember {
            listOf<BottomBarItems?>(
                BottomBarItems(
                    R.drawable.ic_menu_linear,
                    BottomAppBarAction.Menu
                ),
                null,
                null,
                null,
                BottomBarItems(
                    R.drawable.ic_notes,
                    BottomAppBarAction.NoteList
                ),
                BottomBarItems(
                    R.drawable.ic_bookmark_linear,
                    BottomAppBarAction.BookmarkedNoteList
                ),
            )


        }
        Scaffold(
            modifier = Modifier
                .fillMaxSize(),
            bottomBar = {
                HomeBottomAppBar(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(60.dp),
                    bottomBarItems,
                    selectedItem = selectedItem,
                    onItemClicked = { action ->
                        if (action != BottomAppBarAction.Menu)
                            selectedItem = action.action
                        coroutineScope.launch {
                            handleBottomBarSelection(
                                pagerState,
                                drawerState,
                                action
                            )
                        }
                    }
                )

            },
            floatingActionButton = {
                FloatingActionButton(onClick = {
                    navController.navigate(AddNoteScreen)
                }, containerColor = DayNightSecondary) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_add_note_linear),
                        contentDescription = null,
                        tint = Black
                    )
                }
            }
        ) { paddingValues ->
            val bottomPadding = paddingValues.calculateBottomPadding()
            HorizontalPager(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(bottom = bottomPadding)
                    .navigationBarsPadding(),
                state = pagerState, userScrollEnabled = false
            ) { page ->
                when (page) {
                    0 -> {
                        val viewModel: NoteListViewModel = hiltViewModel()
                        viewModel.navController = navController
                        viewModel.updateNoteWidget?.let {
                            if (it) {
                                coroutineScope.launch {
                                    val glanceAppWidget = MyAppWidget()
                                    val glanceId =
                                        GlanceAppWidgetManager(context = context).getGlanceIds(
                                            glanceAppWidget::class.java
                                        )
                                            .firstOrNull()

                                    if (glanceId != null) {
                                        val list = Gson().toJson(viewModel.state.noteList)
                                        updateAppWidgetState(
                                            context = context,
                                            PreferencesGlanceStateDefinition,
                                            glanceId
                                        ) { prefs ->
                                            prefs.toMutablePreferences()
                                            prefs.toMutablePreferences().apply {
                                                set(stringPreferencesKey("notes"), list)
                                            }
                                        }
                                        glanceAppWidget.update(context, glanceId)
                                    }
                                }
                            }
                            viewModel.onEvent(NoteListEvents.RemoveUpdateWidgetState)
                        }
                        if (viewModel.sideEffect != null) {
                            Toast.makeText(
                                LocalContext.current,
                                stringResource(id = viewModel.sideEffect!!),
                                Toast.LENGTH_LONG
                            ).show()
                            viewModel.onEvent(NoteListEvents.RemoveSideEffect)
                        }

                        viewModel.openAlertDialog.also {
                            if (it.first)
                                NormalAlertDialog(
                                    onDismissRequest = {
                                        viewModel.onEvent(NoteListEvents.DismissDeleteDialog)
                                    },
                                    onConfirmation = {
                                        it.second?.let {
                                            viewModel.onEvent(
                                                NoteListEvents.DeleteNote(
                                                    it
                                                )
                                            )
                                        }

                                    },
                                    dialogTitle = stringResource(id = R.string.warning),
                                    dialogText = stringResource(id = R.string.you_are_about_to_delete_note_are_you_sure)
                                )
                        }

                        NoteListScreen(
                            modifier = Modifier.fillMaxSize(),
                            state = viewModel.state,
                            onEvents = viewModel::onEvent
                        )
                    }

                    1 -> {
                        val viewModel: BookmarkedNoteListViewModel = hiltViewModel()
                        viewModel.navController = navController
                        BookmarkedNoteList(state = viewModel.state, viewModel::onEvent)
                    }
                }
            }
        }
    }
}


suspend fun handleBottomBarSelection(
    pagerState: PagerState,
    drawerState: DrawerState,
    action: BottomAppBarAction
) {
    when (action) {
        is BottomAppBarAction.NoteList -> {
            pagerState.animateScrollToPage(
                0,
                animationSpec = spring(stiffness = Spring.StiffnessVeryLow)
            )
        }

        is BottomAppBarAction.BookmarkedNoteList -> {
            pagerState.animateScrollToPage(
                1,
                animationSpec = spring(stiffness = Spring.StiffnessVeryLow)
            )
        }

        is BottomAppBarAction.Menu -> {
            drawerState.open()
        }


    }
}

@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun Preview() {
    HomeScreen(navController = rememberNavController(), changeTheme = {})
}


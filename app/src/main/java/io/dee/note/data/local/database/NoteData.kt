package io.dee.note.data.local.database

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import com.google.gson.Gson
import kotlinx.parcelize.Parcelize
import java.util.Calendar

@Entity
@Parcelize
data class NoteData(
    @PrimaryKey(autoGenerate = true)
    val dId: Int = 0,
    var title: String = "",
    var description: String = "",
    var createdAt: Long = 0L,
    var isBookmarked: Boolean = false,
    @ColumnInfo(defaultValue = "0")
    var isArchived: Boolean = false,
    var reminder: NoteReminderData? = null
) : Parcelable {
    @Parcelize
    data class NoteReminderData(
        var dateToRemind: Calendar? = null,
        var repeatable: Boolean = false
    ) : Parcelable
}

class NoteReminderDataTypeConverter {
    @TypeConverter
    fun fromNoteReminderData(noteReminderData: NoteData.NoteReminderData?): String {
        if (noteReminderData == null) {
            return ""
        }

        return Gson().toJson(noteReminderData)
    }

    @TypeConverter
    fun toNoteReminderData(reminderDataString: String?): NoteData.NoteReminderData? {
        if (reminderDataString == null || reminderDataString.isEmpty()) {
            return null
        }

        return Gson().fromJson(reminderDataString, NoteData.NoteReminderData::class.java)
    }
}
package io.dee.note.presentation.ui.activity.main

import PermissionUtils
import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Intent
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.fadeIn
import androidx.compose.runtime.DisposableEffect
import androidx.core.util.Consumer
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.toRoute
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.AndroidEntryPoint
import io.dee.note.R
import io.dee.note.app.GENERAL_NOTIFICATION_CHANNEL
import io.dee.note.app.IS_CONFIGURATION_CHANGED
import io.dee.note.app.REMINDER_NOTIFICATION_CHANNEL
import io.dee.note.presentation.compose.add_note.AddNoteScreen
import io.dee.note.presentation.compose.add_note.AddNoteViewModel
import io.dee.note.presentation.compose.archive_notes.ArchivedNoteListViewModel
import io.dee.note.presentation.compose.archive_notes.ArchivedNoteScreen
import io.dee.note.presentation.compose.editNote.EditNoteEvents
import io.dee.note.presentation.compose.editNote.EditNoteFragment
import io.dee.note.presentation.compose.editNote.EditNoteViewModel
import io.dee.note.presentation.compose.home_screen.AddNoteScreen
import io.dee.note.presentation.compose.home_screen.ArchivedNoteScreen
import io.dee.note.presentation.compose.home_screen.EditNoteScreen
import io.dee.note.presentation.compose.home_screen.HomeScreen
import io.dee.note.presentation.ui.theme.DeeNoteTheme
import io.dee.note.utils.IntentActionUtil
import io.dee.note.utils.SharedPreferencesUtils
import io.dee.note.utils.UiModeHelper
import javax.inject.Inject


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    companion object {
        const val KEY_DESTINATION = "KEY_DESTINATION"
        const val WidgetAction = "WidgetAction"
    }

    @Inject
    lateinit var sharedPreferencesUtils: SharedPreferencesUtils

    @Inject
    lateinit var actionUtil: IntentActionUtil
    val listener = Consumer<Intent> {
        handleNewIntent(it)
    }
    private val viewModel by viewModels<MainViewModel>()
    private lateinit var navController: NavHostController
    private var backPressedOnce = false
    private val onBackPressedCallback = object : OnBackPressedCallback(enabled = true) {
        override fun handleOnBackPressed() {
            if (backPressedOnce) {
                finish() // Exit the app
                return
            }
            backPressedOnce = true
            Toast.makeText(
                this@MainActivity,
                getString(R.string.press_back_again_to_exit), Toast.LENGTH_SHORT
            ).show()
            Handler(Looper.getMainLooper()).postDelayed({
                backPressedOnce = false
            }, 2000) // Delay of 2 seconds
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            DeeNoteTheme(activity = this) {
                navController = rememberNavController()
                NavHost(
                    navController = navController,
                    startDestination = HomeScreen
                ) {
                    composable<HomeScreen>  {
                        HomeScreen(navController = navController) {
                            switchTheme()
                        }
                    }

                    composable<AddNoteScreen>(enterTransition = {
                        fadeIn(animationSpec = tween(1000))
                    }, exitTransition = {
                        fadeOut(animationSpec = tween(1000))
                    }) {
                        val viewModel: AddNoteViewModel = hiltViewModel()
                        AddNoteScreen(viewModel = viewModel, navigateUp = {
                            navController.navigateUp()
                        })
                    }

                    composable<EditNoteScreen>(enterTransition = {
                        fadeIn(animationSpec = tween(1000))
                    }, exitTransition = {
                        fadeOut(animationSpec = tween(1000))
                    })  {
                        val args = it.toRoute<EditNoteScreen>()
                        val viewModel: EditNoteViewModel = hiltViewModel()
                        viewModel.onEvent(EditNoteEvents.SetNote(args.noteId))
                        EditNoteFragment(viewModel = viewModel) {
                            navController.navigateUp()
                        }
                    }

                    composable<ArchivedNoteScreen> (enterTransition = {
                        fadeIn(animationSpec = tween(1000))
                    }, exitTransition = {
                        fadeOut(animationSpec = tween(1000))
                    }) {
                        val viewModel: ArchivedNoteListViewModel = hiltViewModel()
                        val state = viewModel.state
                        viewModel.navController = navController
                        ArchivedNoteScreen(state, viewModel::onEvent,
                            navigateUp = {
                                navController.navigateUp()
                            })
                    }
                }

            }
            DisposableEffect(Unit) {

                addOnNewIntentListener(listener)
                onDispose { removeOnNewIntentListener(listener) }
            }

            intent?.let {
                listener.accept(intent)
            }
        }

        bindBackPressDispatcher()
        bindFirebaseMessaging()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            checkNotificationPermission()
        } else {
            createNotificationChannels()
        }


    }

    private fun handleNewIntent(intent: Intent?) {
        val action = intent?.extras?.getString(WidgetAction) ?: return
        when (action) {
            actionUtil.generateAction(IntentActionUtil.ADD_NOTE) -> {
                navController.navigate(AddNoteScreen)
            }

            actionUtil.generateAction(IntentActionUtil.OPEN_NOTE) -> {
                val noteId = intent.extras?.getInt(KEY_DESTINATION) ?: -1
                if (noteId == -1) return
                navController.navigate(EditNoteScreen(noteId))
            }

            else -> {}
        }
        this.intent.action = ""
        this.intent.replaceExtras(Bundle())
    }

    private fun createNotificationChannels() {
        val generalChannelId = GENERAL_NOTIFICATION_CHANNEL
        val generalChannelName = getString(R.string.general_notification)
        val generalChannelDescription = ""

        val generalChannel =
            NotificationChannel(
                generalChannelId,
                generalChannelName,
                NotificationManager.IMPORTANCE_HIGH
            )
        generalChannel.enableVibration(true)
        generalChannel.enableLights(true)
        generalChannel.description = generalChannelDescription
        val reminderChannelId = REMINDER_NOTIFICATION_CHANNEL
        val reminderChannelName = getString(R.string.note_reminders)
        val reminderChannelDescription = getString(R.string.channel_for_notes_to_be_reminded)

        val reminderChannel =
            NotificationChannel(
                reminderChannelId,
                reminderChannelName,
                NotificationManager.IMPORTANCE_HIGH
            )
        reminderChannel.description = reminderChannelDescription
        reminderChannel.enableVibration(true)
        reminderChannel.enableLights(true)
        val notificationManager = this.getSystemService(NotificationManager::class.java)

        notificationManager.createNotificationChannels(listOf(generalChannel, reminderChannel))
    }

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    private fun checkNotificationPermission() {
        if (PermissionUtils(this).hasPermission(Manifest.permission.POST_NOTIFICATIONS)) {
            createNotificationChannels()
            return
        }
        PermissionUtils(this).requestPermission(
            Manifest.permission.POST_NOTIFICATIONS,
            onGranted = {
                createNotificationChannels()
            },
            onDenied = {

            })
    }


    private fun bindFirebaseMessaging() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("firebase_token", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result
            Log.d("firebase_token", token)
        })
    }

    private fun bindBackPressDispatcher() {
        onBackPressedDispatcher.addCallback(onBackPressedCallback)
    }


    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        handleNewIntent(intent)
    }

    private fun switchTheme() {
        sharedPreferencesUtils.setBoolean(
            IS_CONFIGURATION_CHANGED,
            true
        )

        when (UiModeHelper.getUiMode(this)) {
            Configuration.UI_MODE_NIGHT_NO -> {

                UiModeHelper.setUiMode(
                    this, Configuration.UI_MODE_NIGHT_YES
                )
            }

            Configuration.UI_MODE_NIGHT_YES -> {
                UiModeHelper.setUiMode(
                    this, Configuration.UI_MODE_NIGHT_NO
                )
            }

            else -> {}
        }
    }
}
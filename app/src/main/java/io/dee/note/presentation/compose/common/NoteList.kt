package io.dee.note.presentation.compose.common

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.lazy.staggeredgrid.LazyVerticalStaggeredGrid
import androidx.compose.foundation.lazy.staggeredgrid.StaggeredGridCells
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import io.dee.note.data.local.database.NoteData
import io.dee.note.presentation.compose.note_list.NoteListEvents


@Composable
fun NoteList(
    modifier: Modifier = Modifier,
    list: List<NoteData>,
    navigateToNoteDetails: (NoteData) -> Unit,
    onBookmarkClicked: (NoteData) -> Unit,
    showMoreOption: (NoteListEvents) -> Unit,
) {


    LazyVerticalStaggeredGrid(
        modifier = modifier,
        columns = StaggeredGridCells.Fixed(2),
        verticalItemSpacing = 10.dp,
        horizontalArrangement = Arrangement.spacedBy(10.dp)
    ) {
        items(list.size) { index ->
            NoteItem(
                note = list[index],
                navigateToNoteDetails = {
                    navigateToNoteDetails(list[index])

                },
                onBookmarkClicked = {
                    onBookmarkClicked(list[index].copy())
                },
                showMoreOption = showMoreOption
            )
        }
    }
}
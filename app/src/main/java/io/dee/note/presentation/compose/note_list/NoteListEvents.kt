package io.dee.note.presentation.compose.note_list

import io.dee.note.data.local.database.NoteData

sealed class NoteListEvents {
    data class SearchNote(val searchQuery: String) : NoteListEvents()
    data class DeleteNote(val note: NoteData) : NoteListEvents()
    data class ShowDeleteDialog(val note: NoteData) : NoteListEvents()
    data object DismissDeleteDialog : NoteListEvents()
    data class Bookmark(val note: NoteData) : NoteListEvents()
    data class EditNote(val note: NoteData) : NoteListEvents()
    data class ArchiveNote(val note: NoteData) : NoteListEvents()
    data object RemoveSideEffect : NoteListEvents()
    data object RemoveUpdateWidgetState : NoteListEvents()
}
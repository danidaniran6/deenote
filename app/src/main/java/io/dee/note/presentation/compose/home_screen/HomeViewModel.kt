package io.dee.note.presentation.compose.home_screen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import io.dee.note.data.local.database.NoteData
import io.dee.note.data.repository.note.NoteListRepository
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val repository: NoteListRepository
) : ViewModel() {

    private var _getNoteByIdResult: MutableSharedFlow<NoteData?> = MutableSharedFlow()
    val getNoteByIdResult = _getNoteByIdResult.asSharedFlow()
    fun getNoteById(id: Int) = viewModelScope.launch {
        _getNoteByIdResult.emit(repository.getNoteById(id))
    }

}
package io.dee.note.presentation.ui.activity.main

import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel
@Inject constructor() : ViewModel() {
    private val _appIntent: MutableLiveData<Intent?> = MutableLiveData()
    val appIntent: LiveData<Intent?> get() = _appIntent

    fun setAppIntent(intent: Intent?) = viewModelScope.launch {
        _appIntent.value = intent
    }
}
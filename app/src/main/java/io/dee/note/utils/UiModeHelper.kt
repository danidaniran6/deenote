package io.dee.note.utils

import android.content.Context
import android.content.res.Configuration
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import io.dee.note.R
import io.dee.note.data.local.enums.ThemeEnum

/** this  object class is used to handle dynamic ui mode dark/ light/ system default
 * stores the last selected ui mode*/

object UiModeHelper {
    private const val PREFS_NAME = "UiModePrefs"
    private const val KEY_UI_MODE = "uiMode"

    fun initUiMode(context: Context) {
        val sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        val savedUiMode = sharedPreferences.getInt(KEY_UI_MODE, -1)
        if (savedUiMode == -1) {
            val systemUiMode =
                context.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
            AppCompatDelegate.setDefaultNightMode(
                when (systemUiMode) {
                    Configuration.UI_MODE_NIGHT_YES -> AppCompatDelegate.MODE_NIGHT_YES
                    else -> AppCompatDelegate.MODE_NIGHT_NO
                }
            )
            sharedPreferences.edit().putInt(KEY_UI_MODE, systemUiMode).apply()
        } else {
            AppCompatDelegate.setDefaultNightMode(
                when (savedUiMode) {
                    Configuration.UI_MODE_NIGHT_YES -> AppCompatDelegate.MODE_NIGHT_YES
                    else -> AppCompatDelegate.MODE_NIGHT_NO
                }
            )
        }
    }

    fun getUiMode(context: Context): Int {
        return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
            .getInt(KEY_UI_MODE, -1)
    }

    fun getThemeMode(context: Context): ThemeEnum {
        return when (getUiMode(context)) {
            Configuration.UI_MODE_NIGHT_YES -> {
                ThemeEnum.Dark
            }

            else -> {
                ThemeEnum.Light
            }
        }

    }

    fun setUiMode(activity: AppCompatActivity, uiMode: Int, delayMillis: Long = 300) {
        Handler(Looper.getMainLooper()).postDelayed({
            activity.supportActionBar?.hide() // optional: hide action bar during animation
            activity.window.setWindowAnimations(R.style.Theme_DeeNote)
            AppCompatDelegate.setDefaultNightMode(
                when (uiMode) {
                    Configuration.UI_MODE_NIGHT_YES -> AppCompatDelegate.MODE_NIGHT_YES
                    else -> AppCompatDelegate.MODE_NIGHT_NO
                }
            )
            activity.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
                .edit().putInt(KEY_UI_MODE, uiMode).apply()
            activity.supportActionBar?.show() // optional: show action bar after animation
        }, delayMillis)
    }
}

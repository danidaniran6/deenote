package io.dee.note.utils

import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

object CustomExceptionHandler {

    @OptIn(DelicateCoroutinesApi::class)
    fun Exception.handleException() {
        GlobalScope.launch(Dispatchers.IO) {
            withContext(Dispatchers.IO) {
                try {
                    this@handleException.printStackTrace()
                } catch (e: Exception) {
                    e.handleException()
                }
            }
        }
    }

    inline fun catcher(crossinline tryBlock: () -> Unit, crossinline catchBlock: () -> Unit) =
        try {
            tryBlock()
        } catch (e: Exception) {
            catchBlock()
            e.handleException()
        }

    inline fun catcher(crossinline tryBlock: () -> Unit) =
        try {
            tryBlock()
        } catch (e: Exception) {
            e.handleException()
        }
}
package io.dee.note.presentation.compose.note_list

import io.dee.note.data.local.database.NoteData

data class NoteListState(
    var searchQuery: String = "",
    var noteList: List<NoteData> = emptyList()
)

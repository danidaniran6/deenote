package io.dee.note.presentation.compose.home_screen

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
sealed class BottomAppBarAction(val action: String) : Parcelable {
    data object NoteList : BottomAppBarAction("NoteList")
    data object BookmarkedNoteList : BottomAppBarAction("BookmarkedNoteList")
    data object Menu : BottomAppBarAction("Menu")
}

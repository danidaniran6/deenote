package io.dee.note.presentation.ui.theme

import android.app.Activity
import android.os.Build
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.dynamicDarkColorScheme
import androidx.compose.material3.dynamicLightColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalView
import androidx.core.view.WindowCompat
import androidx.glance.material3.ColorProviders
import io.dee.composenewsapp.ui.theme.DarkPrimary
import io.dee.composenewsapp.ui.theme.DarkSecondary
import io.dee.composenewsapp.ui.theme.Primary
import io.dee.composenewsapp.ui.theme.Red
import io.dee.composenewsapp.ui.theme.Secondary

private val DarkColorScheme = darkColorScheme(
    primary = DarkPrimary,
    secondary = DarkSecondary,
    error = Red
)

private val LightColorScheme = lightColorScheme(
    primary = Primary,
    secondary = Secondary,
    error = Red
)


object MyAppWidgetGlanceColorScheme {

    val colors = ColorProviders(
        light = LightColorScheme.copy(background = Color.Black),
        dark = DarkColorScheme.copy(background = Color.White)
    )
}


@Composable
fun DeeNoteTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    // Dynamic color is available on Android 12+
    dynamicColor: Boolean = false,
    activity: Activity,
    content: @Composable () -> Unit
) {
    val colorScheme = when {
        dynamicColor && Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> {
            val context = LocalContext.current
            if (darkTheme) dynamicDarkColorScheme(context) else dynamicLightColorScheme(context)
        }

        darkTheme -> DarkColorScheme
        else -> LightColorScheme
    }
    val view = LocalView.current
    if (!view.isInEditMode) {
        SideEffect {
            val window = activity.window
            window.statusBarColor =
                (if (darkTheme) Color(0XFF302F33) else Color(0XFFF0F0F2)).toArgb()
            WindowCompat.getInsetsController(window, view).isAppearanceLightStatusBars = !darkTheme
        }
    }

    MaterialTheme(
        colorScheme = colorScheme,
        typography = Typography,
        content = content
    )
}
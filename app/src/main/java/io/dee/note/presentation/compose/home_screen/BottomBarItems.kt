package io.dee.note.presentation.compose.home_screen

import android.os.Parcelable
import androidx.annotation.DrawableRes
import kotlinx.parcelize.Parcelize

@Parcelize
data class BottomBarItems(
    @DrawableRes val icon: Int,
    val action: BottomAppBarAction? = null
) : Parcelable


package io.dee.note.data.repository.note

import io.dee.note.data.local.database.AppDb
import io.dee.note.data.local.database.NoteData
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ArchivedNoteListRepositoryImpl
@Inject constructor(
    private val appDb: AppDb
) : ArchivedNoteListRepository {
    override suspend fun getAllArchivedNoteListAsAList(): List<NoteData> {
        return appDb.noteDataDao.getAllArchivedNotesAsAList()
    }

    override fun getAllArchivedNoteListAsAFlow(): Flow<List<NoteData>> {
        return appDb.noteDataDao.getAllArchivedNotesAsAFlow()
    }

    override suspend fun upsertNote(note: NoteData) {
        appDb.noteDataDao.upsertNote(note)
    }

}
package io.dee.note.presentation.compose.archive_notes

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import io.dee.note.presentation.compose.archive_notes.components.ArchiveNoteList
import io.dee.note.presentation.compose.note_list.NoteListEvents
import io.dee.note.presentation.compose.note_list.NoteListState

@Composable
fun ArchivedNoteScreen(
    state: NoteListState,
    onEvent: (NoteListEvents) -> Unit,
    navigateUp: () -> Unit
) {
    Scaffold(modifier = Modifier
        .fillMaxSize()
        .statusBarsPadding(), topBar = {
        ArchiveNoteTopBar(
            modifier = Modifier
                .fillMaxWidth()
                .statusBarsPadding()
                .padding(10.dp),
            navigateUp = {
                navigateUp()
            }
        )
    }) { paddingValues ->
        val topPadding = paddingValues.calculateTopPadding()
        ArchiveNoteList(
            modifier = Modifier
                .fillMaxSize()
                .padding(top = topPadding)
                .statusBarsPadding(),
            state = state,
            onEvents = onEvent
        )
    }
}
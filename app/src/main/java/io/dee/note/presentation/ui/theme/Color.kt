package io.dee.composenewsapp.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

val DayNightPrimary: Color
    @Composable
    get() {
        return if (isSystemInDarkTheme()) Color(0xFF1B1A1E) else Color(0XFFFAFAFA)
    }
val Primary = Color(0XFFFAFAFA)
val DarkPrimary = Color(0XFF1B1A1E)

val DayNightSecondary: Color
    @Composable
    get() {
        return if (isSystemInDarkTheme()) Color(0XFF302F33) else Color(0XFFF0F0F2)
    }


val Secondary = Color(0XFFF0F0F2)
val DarkSecondary = Color(0XFF302F33)


val Red = Color(0XFFE53935)

val Black: Color
    @Composable
    get() {
        return if (isSystemInDarkTheme()) Color.White else Color.Black
    }
val White: Color
    @Composable
    get() {
        return if (isSystemInDarkTheme()) Color.Black else Color.White
    }
package io.dee.note.data.repository.note

import io.dee.note.data.local.database.AppDb
import io.dee.note.data.local.database.NoteData
import kotlinx.coroutines.flow.Flow

class NoteListRepositoryImpl(
    private val appDb: AppDb
) : NoteListRepository {
    override suspend fun upsertNote(note: NoteData) {
        appDb.noteDataDao.upsertNote(note)
    }

    override suspend fun getAllNotesAsAList(): List<NoteData> {
        return appDb.noteDataDao.getAllNotesAsAList()
    }

    override suspend fun deleteTheInvalidNote() {
        appDb.noteDataDao.deleteTheInvalidNote()
    }

    override fun getAllNotesAsAFlow(): Flow<List<NoteData>> {
        return appDb.noteDataDao.getAllNotesAsAFlow()
    }

    override suspend fun getNoteById(id: Int): NoteData? {
        return appDb.noteDataDao.getNoteById(id)
    }

    override suspend fun deleteANote(note: NoteData) {
        appDb.noteDataDao.deleteANote(note)
    }

}
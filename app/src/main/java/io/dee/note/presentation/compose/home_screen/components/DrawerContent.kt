package io.dee.note.presentation.compose.home_screen.components

import android.content.res.Configuration
import androidx.annotation.DrawableRes
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.LineBreak
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.dp
import io.dee.composenewsapp.ui.theme.Black
import io.dee.composenewsapp.ui.theme.DayNightSecondary
import io.dee.note.R
import io.dee.note.presentation.compose.home_screen.DrawerEvents
import io.dee.note.presentation.ui.theme.deeTypography

@Composable
fun DrawerContent(
    modifier: Modifier = Modifier,
    onEvent: (DrawerEvents) -> Unit
) {
    Column(modifier = modifier) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .background(
                    DayNightSecondary,
                    shape = MaterialTheme.shapes.medium
                )
        ) {
            Text(
                modifier = Modifier.padding(10.dp),
                text = stringResource(id = R.string.drawer_header_text),
                style = deeTypography.copy(
                    fontSize = TextUnit(16f, TextUnitType.Sp),
                    lineBreak = LineBreak.Simple,
                ),
                textAlign = TextAlign.Start,
                lineHeight = TextUnit(1.5f, TextUnitType.Em),
                color = Black
            )
        }
        Spacer(modifier = Modifier.weight(1f))
        Column {
            DrawerButton(
                text = stringResource(id = R.string.archive),
                icon = R.drawable.ic_archive_linear,
                onClick = { onEvent(DrawerEvents.NavigateToArchive) }
            )
            Spacer(modifier = Modifier.height(10.dp))
            DrawerButton(
                text = stringResource(id = R.string.theme),
                icon = if (!isSystemInDarkTheme()) R.drawable.ic_moon_linear else R.drawable.ic_sun_linear,
                onClick = { onEvent(DrawerEvents.ChangeTheme) }
            )

            Spacer(modifier = Modifier.height(10.dp))
            val versionCode = LocalContext.current.packageManager.getPackageInfo(
                LocalContext.current.packageName, 0
            )?.versionName.toString()
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 20.dp),
                text = stringResource(
                    id = R.string.app_name_app_version,
                    versionCode
                ),
                style = deeTypography,
                color = Black,
                textAlign = TextAlign.Center
            )
        }

    }
}


@Composable
fun DrawerButton(
    modifier: Modifier = Modifier,
    text: String,
    @DrawableRes icon: Int,
    onClick: () -> Unit
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .background(
                DayNightSecondary,
                shape = MaterialTheme.shapes.medium
            )
            .clickable { onClick() }
            .then(modifier)
    ) {
        Row(
            modifier = Modifier.padding(15.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                modifier = Modifier.weight(1f),
                text = text,
                style = deeTypography.copy(fontWeight = FontWeight.Bold),
                color = Black
            )
            Icon(
                painter = painterResource(id = icon),
                contentDescription = null,
                tint = Black
            )
        }
    }
}

@Preview(showBackground = true)
@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun Preview() {
    DrawerContent(
        onEvent = {}
    )
}
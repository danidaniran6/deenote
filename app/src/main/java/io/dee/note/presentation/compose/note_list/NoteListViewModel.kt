package io.dee.note.presentation.compose.note_list

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import dagger.hilt.android.lifecycle.HiltViewModel
import io.dee.note.R
import io.dee.note.data.local.database.NoteData
import io.dee.note.data.repository.note.NoteListRepository
import io.dee.note.presentation.compose.home_screen.EditNoteScreen
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NoteListViewModel
@Inject constructor(
    private val repository: NoteListRepository
) : ViewModel() {


    var navController by mutableStateOf<NavController?>(null)

    var state by mutableStateOf(NoteListState())
        private set

    var sideEffect by mutableStateOf<Int?>(null)
        private set
    var updateNoteWidget by mutableStateOf<Boolean?>(null)
        private set

    var openAlertDialog by mutableStateOf(Pair<Boolean, NoteData?>(false, null))
        private set

    fun onEvent(events: NoteListEvents) {
        when (events) {
            is NoteListEvents.SearchNote -> {
                searchNote(events.searchQuery)
            }

            is NoteListEvents.Bookmark -> {
                makeNoteFavourite(events.note)
            }

            is NoteListEvents.ShowDeleteDialog -> {
                openAlertDialog = Pair(true, events.note)
            }

            is NoteListEvents.DismissDeleteDialog -> {
                openAlertDialog = Pair(false, null)
            }

            is NoteListEvents.DeleteNote -> {
                onEvent(NoteListEvents.DismissDeleteDialog)
                deleteANote(events.note)
            }

            is NoteListEvents.ArchiveNote -> {
                archiveNote(events.note)
            }

            is NoteListEvents.RemoveSideEffect -> {
                sideEffect = null
            }

            is NoteListEvents.RemoveUpdateWidgetState -> {
                updateNoteWidget = null
            }

            is NoteListEvents.EditNote -> {
                navController?.navigate(
                    EditNoteScreen(
                        events.note.dId
                    )
                )
            }
        }

    }


    init {
        getAllNotes()
    }

    private fun getAllNotes(searchQuery: String = "") = viewModelScope.launch {
        repository.getAllNotesAsAFlow().onEach {
            if (it.any { it.title.isEmpty() && it.description.isEmpty() }) {
                sideEffect = R.string.empty_note_discarded
                deleteTheInvalidNote()
            } else {
                val list = if (searchQuery.isNotEmpty()) it.filter {
                    it.title.contains(
                        searchQuery,
                        true
                    ) || it.description.contains(searchQuery, true)
                } else it
                state = state.copy(noteList = list, searchQuery = searchQuery)
                updateNoteWidget = true
            }

        }.launchIn(viewModelScope)
    }

    private fun deleteTheInvalidNote() = viewModelScope.launch {
        repository.deleteTheInvalidNote()
    }

    private fun makeNoteFavourite(note: NoteData) = viewModelScope.launch {
        repository.upsertNote(note.apply {
            this.isBookmarked = !isBookmarked
        })
    }

    private fun archiveNote(note: NoteData) = viewModelScope.launch {
        repository.upsertNote(note.apply {
            this.isArchived = !isArchived
        })
    }

    private fun deleteANote(note: NoteData) = viewModelScope.launch {
        repository.deleteANote(note)
    }

    private fun searchNote(searchQuery: String) {
        getAllNotes(searchQuery)
    }


}
package io.dee.note.data.repository.note

import io.dee.note.data.local.database.NoteData

interface EditNoteRepository {
    suspend fun deleteANote(note: NoteData)
    suspend fun upsertNote(note: NoteData)
    suspend fun getNoteById(noteId: Int): NoteData?
}